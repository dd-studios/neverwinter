import java.time.Instant
import java.time.format.DateTimeFormatter

plugins {
    `java-library`
    `maven-publish`
    id("com.github.hierynomus.license") version "0.16.1"
    id("com.github.ben-manes.versions") version "0.42.0"
    id("org.ajoberstar.grgit.service") version "5.0.0"
    id("org.javamodularity.moduleplugin") version "1.8.12"
}

val gitCommit: String by lazy { grgitService.service.get().grgit.head()?.id ?: "<unknown commit>" }
val buildString: String? by lazy { System.getProperty("BUILD_NUMBER")?.let { "+build.$it" } ?: "" }

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(JavaVersion.VERSION_17.majorVersion))
    withSourcesJar()
}

license {
    header = file("LICENSE")
    strictCheck = true
    mapping("java", "SLASHSTAR_STYLE")
}

group = "com.dwarveddonuts.neverwinter"
version = "1.0.0"

repositories {
    mavenCentral()
}

dependencies {}

publishing {
    publications {
        register<MavenPublication>("mavenJava") {
            from(components["java"])
        }
    }
    repositories {
        if ("true" == (System.getenv("CI") ?: "")) {
            val apiUrl = System.getenv("CI_API_V4_URL")!!
            val jobToken = System.getenv("CI_JOB_TOKEN")!!
            val projectId = System.getenv("CI_PROJECT_ID")!!

            maven("$apiUrl/projects/$projectId/packages/maven") {
                name = "neverwinter"
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = jobToken
                }
                authentication {
                    register<HttpHeaderAuthentication>("header")
                }
            }
        }
    }
}

tasks {
    withType<Jar> {
        manifest {
            attributes(
                "Specification-Title" to "NeverWinter",
                "Specification-Version" to project.version,
                "Specification-Vendor" to "Dwarved Donuts Studios",
                "Implementation-Title" to "NeverWinter",
                "Implementation-Version" to "${project.version}$buildString",
                "Implementation-Vendor" to "Dwarved Donuts Studios",
                "Implementation-Timestamp" to DateTimeFormatter.ISO_INSTANT.format(Instant.now()),
                "Implementation-CommitTarget" to gitCommit
            )
        }
    }

    withType<Wrapper> {
        gradleVersion = "7.3"
        distributionType = Wrapper.DistributionType.ALL
    }
}
