/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/**
 * Provides a set of utilities to ease usage of {@link java.lang.invoke.MethodHandle}s and
 * {@link java.lang.invoke.VarHandle}s.
 *
 * <p>The {@code Handle} API provided by Java is meant to be an alternative to reflection with respect to access to
 * methods and fields in classes. Usage of handles usually leads to faster performing code, thanks to access checks
 * being performed at creation time rather than at every invocation and polymorphic method signatures, but is also
 * plagued by a more low-level and thus more verbose API.</p>
 *
 * <p>The goal of this set of utilities is to allow for a more streamlined creation and usage of handles, through the
 * methods provided by {@link com.dwarveddonuts.neverwinter.handle.Handles}. It also allows some additional capabilities
 * in the form of full unrestricted reflection (refer to
 * {@link com.dwarveddonuts.neverwinter.handle.RequestType#trusted()}) and the conversion of checked exceptions such
 * as {@link java.lang.IllegalAccessException} into more meaningful unchecked exceptions, to reduce code verbosity.</p>
 *
 * <p>This component does not require other components to work, unless the client makes use of one of the methods in
 * the following list: {@link com.dwarveddonuts.neverwinter.handle.RequestType#trusted()},
 * {@link com.dwarveddonuts.neverwinter.handle.RequestType#standard()}, and
 * {@link com.dwarveddonuts.neverwinter.handle.ThrowableWrapper#rethrowSneakily()}. In this case, functionality of the
 * {@link com.dwarveddonuts.neverwinter.unsafe} component will be used.</p>
 *
 * @since 1.0.0
 */
package com.dwarveddonuts.neverwinter.handle;
