/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.handle;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.VarHandle;
import java.util.Objects;

/**
 * Represents the type of access that should be used to obtain a handle by the methods in {@link Handles}.
 *
 * <p>The access determines how {@link MethodHandle}s and {@link VarHandle}s should be looked up to properly account
 * for the various access types available in Java bytecode. Not all access types can be used for both methods and
 * fields. Refer to the documentation of the specific methods for these restrictions.</p>
 *
 * <p>Clients cannot create instances of this class directly. They should instead refer to the static factory methods
 * available.</p>
 *
 * @since 1.0.0
 */
public sealed abstract class AccessType {
    private static sealed abstract class MethodAccessType extends AccessType permits ConstructorAccessType, SpecialAccessType, VirtualAccessType {
        protected MethodAccessType(final String name) {
            super(name);
        }

        @Override
        final VarHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final Class<?> type) {
            throw HandleErrors.unsupported(this, receiver, name, type);
        }
    }

    private static sealed abstract class FieldAccessType extends AccessType permits DirectAccessType {
        protected FieldAccessType(final String name) {
            super(name);
        }

        @Override
        final MethodHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final MethodType type) {
            throw HandleErrors.unsupported(this, receiver, name, type);
        }
    }

    private static final class StaticAccessType extends AccessType {
        static final AccessType INSTANCE = new StaticAccessType();

        private StaticAccessType() {
            super("static");
        }

        @Override
        MethodHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final MethodType type)
                throws IllegalAccessException, NoSuchMethodException {
            return lookup.findStatic(receiver, name, type);
        }

        @Override
        VarHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final Class<?> type)
                throws IllegalAccessException, NoSuchFieldException {
            return lookup.findStaticVarHandle(receiver, name, type);
        }
    }

    private static final class VirtualAccessType extends MethodAccessType {
        static final AccessType INSTANCE = new VirtualAccessType();

        private VirtualAccessType() {
            super("virtual");
        }

        @Override
        MethodHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final MethodType type)
                throws IllegalAccessException, NoSuchMethodException {
            return lookup.findVirtual(receiver, name, type);
        }
    }

    private static final class DirectAccessType extends FieldAccessType {
        static final AccessType INSTANCE = new DirectAccessType();

        private DirectAccessType() {
            super("direct");
        }

        @Override
        VarHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final Class<?> type)
                throws IllegalAccessException, NoSuchFieldException {
            return lookup.findVarHandle(receiver, name, type);
        }
    }

    private static final class SpecialAccessType extends MethodAccessType {
        private final Class<?> caller;

        SpecialAccessType(final Class<?> caller) {
            super("special");
            this.caller = Objects.requireNonNull(caller);
        }

        @Override
        MethodHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final MethodType type)
                throws IllegalAccessException, NoSuchMethodException {
            return lookup.findSpecial(receiver, name, type, this.caller);
        }

        @Override
        public String toString() {
            return super.toString() + " from class " + this.caller.getName();
        }
    }

    private static final class ConstructorAccessType extends MethodAccessType {
        private static final String INIT = "<init>";

        static final AccessType INSTANCE = new ConstructorAccessType();

        private ConstructorAccessType() {
            super("constructor");
        }

        @Override
        MethodHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final MethodType type)
                throws IllegalAccessException, NoSuchMethodException {
            if (!INIT.equals(name)) {
                throw HandleErrors.unsupported(this, receiver, name, type);
            }
            return lookup.findConstructor(receiver, type);
        }
    }

    private final String name;

    protected AccessType(final String name) {
        this.name = name;
    }

    /**
     * Obtains an {@link AccessType} that allows accessing methods and fields statically.
     *
     * <p>This access type can be safely used to reference both {@code static} methods and fields.</p>
     *
     * @return An {@link AccessType} for {@code static} methods and fields.
     *
     * @since 1.0.0
     */
    public static AccessType staticAccess() {
        return StaticAccessType.INSTANCE;
    }

    /**
     * Obtains an {@link AccessType} that allows accessing methods virtually.
     *
     * <p>A virtual method is any {@code public}, package-private, or {@code protected} method that is not
     * {@code static}.</p>
     *
     * <p>This access type cannot be used for fields. If the client wants to access a non-{@code static} field, use
     * {@link #directAccess()} instead.</p>
     *
     * @return An {@link AccessType} for virtual methods.
     *
     * @since 1.0.0
     */
    public static AccessType virtualAccess() {
        return VirtualAccessType.INSTANCE;
    }

    /**
     * Obtains an {@link AccessType} that allows accessing non-{@code static} fields.
     *
     * <p>Direct access is used as virtual access is not available for fields, as they can only be shadowed and not
     * overridden.</p>
     *
     * <p>This access type cannot be used for methods. If the client wants to access a virtual method (i.e. a
     * non-{@code static} {@code public}, package-private, or {@code protected} method), refer to
     * {@link #virtualAccess()}. If, on the other hand, the client wants to access a {@code private} method, then
     * special access is needed through {@link #specialAccess(Class)}.</p>
     *
     * @return An {@link AccessType} for non-{@code static} fields.
     *
     * @since 1.0.0
     */
    public static AccessType directAccess() {
        return DirectAccessType.INSTANCE;
    }

    /**
     * Obtains an {@link AccessType} that allows accessing methods with special access.
     *
     * <p>Methods accessed through special access will bypass overriding relationships and will be invoked directly.
     * Only {@code private} methods may be accessed safely through this access.</p>
     *
     * <p>This access type cannot be used for fields. Moreover, even though constructors are invoked through special
     * access instructions, they cannot be accessed with this {@code AccessType}. Clients that wish to access
     * constructors should thus use the special {@link #constructorAccess()} access type.</p>
     *
     * @param caller The {@link Class} that will invoke the target methods specially.
     * @return An {@link AccessType} that allows special access through the given {@link Class}.
     * @throws NullPointerException If {@code caller} is {@code null}.
     *
     * @since 1.0.0
     */
    public static AccessType specialAccess(final Class<?> caller) {
        return new SpecialAccessType(caller);
    }

    /**
     * Obtains an {@link AccessType} that allows accessing solely constructors.
     *
     * <p>Fields cannot be accessed through this access type.</p>
     *
     * <p>Attempting to use constructor access to access any methods that are not the constructor (i.e., {@code <init>})
     * will result in an error being thrown during access.</p>
     *
     * @return An {@link AccessType} for constructor access.
     *
     * @since 1.0.0
     */
    public static AccessType constructorAccess() {
        return ConstructorAccessType.INSTANCE;
    }

    final MethodHandle access(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final MethodType type) {
        try {
            return this.lookup(lookup, receiver, name, type);
        } catch (final IllegalAccessException e) {
            throw HandleErrors.noAccess(this, receiver, name, type, e);
        } catch (final NoSuchMethodException e) {
            throw HandleErrors.missing(receiver, name, type, e);
        }
    }

    final VarHandle access(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final Class<?> type) {
        try {
            return this.lookup(lookup, receiver, name, type);
        } catch (final IllegalAccessException e) {
            throw HandleErrors.noAccess(this, receiver, name, type, e);
        } catch (final NoSuchFieldException e) {
            throw HandleErrors.missing(receiver, name, type, e);
        }
    }

    abstract MethodHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final MethodType type)
            throws IllegalAccessException, NoSuchMethodException;

    abstract VarHandle lookup(final MethodHandles.Lookup lookup, final Class<?> receiver, final String name, final Class<?> type)
            throws IllegalAccessException, NoSuchFieldException;

    @Override
    public String toString() {
        return this.name;
    }
}
