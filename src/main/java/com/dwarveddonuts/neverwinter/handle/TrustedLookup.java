/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.handle;

import com.dwarveddonuts.neverwinter.unsafe.FieldAddress;
import com.dwarveddonuts.neverwinter.unsafe.TypedUnsafe;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.Field;
import java.util.function.Supplier;

final class TrustedLookup {
    private static final class LazyHelper {
        static final MethodHandles.Lookup LOOKUP = ((Supplier<MethodHandles.Lookup>) () -> {
            final Class<?> methodHandlesLookupClass = MethodHandles.Lookup.class;

            // This is the optimistic approach: we know IMPL_LOOKUP exists, so we try to obtain it. If we cannot, then
            // we try to find an equivalent field with all modes we care about.
            try {
                final Field implLookup = methodHandlesLookupClass.getDeclaredField("IMPL_LOOKUP");
                final FieldAddress<MethodHandles.Lookup> address = TypedUnsafe.fieldAddress(implLookup);
                return TypedUnsafe.get(address);
            } catch (final Exception e) {

                final Field[] declaredFields = methodHandlesLookupClass.getDeclaredFields();

                for (final Field declaredField : declaredFields) {
                    if (declaredField.getType() != methodHandlesLookupClass) {
                        continue;
                    }

                    final FieldAddress<MethodHandles.Lookup> address = TypedUnsafe.fieldAddress(declaredField);
                    final MethodHandles.Lookup lookup = TypedUnsafe.get(address);

                    if (lookup.lookupModes() != 127) {
                        continue;
                    }

                    return lookup;
                }
            }

            throw new UnableToObtainLookupException("Unable to find trusted lookup", new NoSuchFieldException());
        }).get();
    }

    private TrustedLookup() {}

    static MethodHandles.Lookup lookup() {
        return LazyHelper.LOOKUP;
    }
}
