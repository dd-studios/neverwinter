/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.handle;

/**
 * Thrown whenever invocation of a {@link java.lang.invoke.MethodHandle} through a {@link HandleInvoker} fails.
 *
 * <p>Note that invocation failure is determined as the {@code MethodHandle} being unable to correctly resolve the
 * method, e.g. when a {@link java.lang.invoke.WrongMethodTypeException} is thrown. Invocation of a method that
 * results in an exception being thrown due to normal method execution is <strong>not</strong> considered invocation
 * failure.</p>
 *
 * @since 1.0.0
 */
public final class HandleInvocationFailedException extends RuntimeException {
    HandleInvocationFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
