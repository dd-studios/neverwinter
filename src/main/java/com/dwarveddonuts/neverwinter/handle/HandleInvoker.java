/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.handle;

/**
 * Invokes a particular {@link java.lang.invoke.MethodHandle}, returning its result.
 *
 * <p>This interface is essentially a {@link java.util.function.Supplier} that is allowed to raise a {@link Throwable}
 * to properly wrap {@link java.lang.invoke.MethodHandle#invokeExact(Object...)} and other invocation methods in a
 * lambda for usage within {@link Handles}.</p>
 *
 * <p>This is a {@linkplain FunctionalInterface functional interface} whose functional method is {@link #invoke()}.</p>
 *
 * @param <T> The type returned by the {@link java.lang.invoke.MethodHandle}.
 *
 * @since 1.0.0
 */
@FunctionalInterface
public interface HandleInvoker<T> {
    /**
     * Invokes a particular {@link java.lang.invoke.MethodHandle}, returning nothing.
     *
     * <p>This is a specialization of {@link HandleInvoker} for invocation of {@code void} methods.</p>
     *
     * <p>This is a {@linkplain FunctionalInterface functional interface} whose functional method is
     * {@link #invokeVoid()}.</p>
     *
     * @since 1.0.0
     */
    @FunctionalInterface
    interface Void extends HandleInvoker<Void> {
        /**
         * Invokes a particular {@link java.lang.invoke.MethodHandle}.
         *
         * @throws Throwable In case the invoked handle throws the same exception.
         *
         * @since 1.0.0
         */
        void invokeVoid() throws Throwable;

        @Override
        default Void invoke() throws Throwable {
            this.invokeVoid();
            return null;
        }
    }

    /**
     * Invokes a particular {@link java.lang.invoke.MethodHandle}.
     *
     * @return The result of the invocation.
     * @throws Throwable In case the invoked handle throws the same exception.
     *
     * @since 1.0.0
     */
    T invoke() throws Throwable;
}
