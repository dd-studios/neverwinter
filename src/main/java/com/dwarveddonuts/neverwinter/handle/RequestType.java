/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.handle;

import java.lang.invoke.MethodHandles;
import java.util.Objects;

/**
 * Indicates the type of request that should be performed to obtain a {@link MethodHandles.Lookup} instance.
 *
 * <p>The {@code Lookup} instance determines to which constraints the request is tied, e.g. by only allowing a subset
 * of elements to be discovered or only a subset of operations to be carried out.</p>
 *
 * <p>Clients cannot create instances of this class directly. They should instead refer to the various static factory
 * methods provided.</p>
 *
 * @since 1.0.0
 */
public abstract sealed class RequestType {
    private static final class SpecificRequestType extends RequestType {
        static final RequestType PUBLIC_ONLY = new SpecificRequestType("public only", MethodHandles.publicLookup());
        static final RequestType TRUSTED = new SpecificRequestType("trusted", TrustedLookup.lookup());

        private final MethodHandles.Lookup lookup;

        SpecificRequestType(final String name, final MethodHandles.Lookup lookup) {
            super(name);
            this.lookup = Objects.requireNonNull(lookup);
        }

        @Override
        MethodHandles.Lookup lookupFor(final Class<?> receiver) {
            return this.lookup;
        }
    }

    private static final class OwnerBased extends RequestType {
        static final RequestType STANDARD = new OwnerBased("standard", TrustedLookup.lookup());
        static final RequestType RESTRICTED = new OwnerBased("restricted", MethodHandles.lookup());

        private final MethodHandles.Lookup lookup;

        private OwnerBased(final String name, final MethodHandles.Lookup lookup) {
            super(name);
            this.lookup = Objects.requireNonNull(lookup);
        }

        @Override
        MethodHandles.Lookup lookupFor(final Class<?> receiver) {
            try {
                return MethodHandles.privateLookupIn(receiver, this.lookup);
            } catch (final IllegalAccessException e) {
                throw new UnableToObtainLookupException("Unable to access lookup for " + receiver.getName(), e);
            }
        }
    }

    private final String name;

    protected RequestType(final String name) {
        this.name = name;
    }

    /**
     * Obtains a {@link RequestType} that can only request public elements.
     *
     * <p>All constraints on field's {@code final} modifiers and method access modifiers are respected.</p>
     *
     * @return A public only {@link RequestType}.
     *
     * @since 1.0.0
     */
    public static RequestType publicOnly() {
        return SpecificRequestType.PUBLIC_ONLY;
    }

    /**
     * Obtains a {@link RequestType} that is trusted by the JVM.
     *
     * <p>A trusted request type means that no constraints are placed by the Java runtime on both operations and access,
     * allowing potentially unsafe operations such as changing the value of a {@code final} field or accessing methods
     * outside of module boundaries to occur without issues. Care must be taken when using this request type in client
     * code.</p>
     *
     * <p>Usage of this request type requires access to the {@link com.dwarveddonuts.neverwinter.unsafe} component.</p>
     *
     * @return A trusted {@link RequestType}.
     *
     * @since 1.0.0
     */
    public static RequestType trusted() {
        return SpecificRequestType.TRUSTED;
    }

    /**
     * Obtains a {@link RequestType} that can perform the standard set of operations.
     *
     * <p>A standard request is the one that is commonly available through {@link MethodHandles#lookup()} when executed
     * in the class that is the owner of the target field or method. Access to all methods is thus available and so is
     * inter-module access for every module that the class reads. Potentially unsafe operations such as changing the
     * value of {@code final} fields is not available.</p>
     *
     * <p>Usage of this request type requires access to the {@link com.dwarveddonuts.neverwinter.unsafe} component.</p>
     *
     * @return A standard {@link RequestType}.
     *
     * @since 1.0.0
     */
    public static RequestType standard() {
        return OwnerBased.STANDARD;
    }

    /**
     * Obtains a {@link RequestType} that is restricted on the set of abilities.
     *
     * <p>A restricted request type means that access will be restricted to what is available to the NeverWinter module
     * through normal Java means. In other words, access to opened packages of other modules will be available, but more
     * invasive inter-module access will be restricted. Potentially unsafe operations are also unavailable.</p>
     *
     * <p>This request type is equivalent to normal reflection accesses that would be able to be performed by
     * NeverWinter alone.</p>
     *
     * @return A restricted {@link RequestType}.
     *
     * @since 1.0.0
     */
    public static RequestType restricted() {
        return OwnerBased.RESTRICTED;
    }

    /**
     * Obtains a {@link RequestType} that uses a specific {@link MethodHandles.Lookup} instance.
     *
     * <p>The exact capabilities of the given {@code Lookup} instance will be used, with no privilege escalation
     * attempts possible.</p>
     *
     * @param lookup The {@link MethodHandles.Lookup} instance to use.
     * @return A {@link RequestType} that uses the specified {@link MethodHandles.Lookup}.
     * @throws NullPointerException If the given {@link MethodHandles.Lookup} is {@code null}.
     *
     * @since 1.0.0
     */
    public static RequestType with(final MethodHandles.Lookup lookup) {
        return new SpecificRequestType("custom lookup", Objects.requireNonNull(lookup));
    }

    abstract MethodHandles.Lookup lookupFor(final Class<?> receiver);

    @Override
    public String toString() {
        return this.name;
    }
}
