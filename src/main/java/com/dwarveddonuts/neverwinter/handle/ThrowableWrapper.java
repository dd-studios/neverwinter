/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.handle;

import com.dwarveddonuts.neverwinter.unsafe.TypedUnsafe;

import java.util.Objects;

/**
 * Indicates how a {@link Throwable} thrown by a {@link java.lang.invoke.MethodHandle} invocation should be wrapped.
 *
 * <p>This class is used in conjunction with {@link HandleInvoker}s to determine how the potential {@link Throwable}
 * should be handled in case it gets thrown.</p>
 *
 * <p>Clients cannot create new instances of this class, but can use static factory methods available in this class to
 * obtain instances of it.</p>
 *
 * @since 1.0.0
 */
public sealed abstract class ThrowableWrapper {
    private static final class Wrap extends ThrowableWrapper {
        static final ThrowableWrapper INSTANCE = new Wrap();

        private Wrap() {
            super("wrap");
        }

        @Override
        RuntimeException rethrow(final Throwable t) {
            final String message = Objects.requireNonNullElse(t.getMessage(), t.getClass().getName());
            throw new RuntimeException(message, t);
        }
    }

    private static final class Uncheck extends ThrowableWrapper {
        static final ThrowableWrapper INSTANCE = new Uncheck();

        private Uncheck() {
            super("uncheck");
        }

        @Override
        RuntimeException rethrow(final Throwable t) {
            // throw or return is the same here, using throw for symmetry with the other wrappers
            throw uncheckedRethrow(RuntimeException.class, t);
        }

        @SuppressWarnings("unchecked")
        private static <T extends Throwable> T uncheckedRethrow(
                @SuppressWarnings({"SameParameterValue", "unused"}) final Class<T> clazz,
                final Throwable t
        ) throws T {
            throw (T) t;
        }
    }

    private static final class Sneak extends ThrowableWrapper {
        static final ThrowableWrapper INSTANCE = new Sneak();

        private Sneak() {
            super("sneak");
        }

        @Override
        RuntimeException rethrow(final Throwable t) {
            throw TypedUnsafe.sneakyThrow(t);
        }
    }

    private final String name;

    protected ThrowableWrapper(final String name) {
        this.name = name;
    }

    /**
     * Obtains a {@link ThrowableWrapper} that wraps exceptions.
     *
     * <p>Any {@link Throwable} instance is wrapped into a {@link RuntimeException} and the newly created exception is
     * rethrown, making the checked exception unchecked.</p>
     *
     * <p>This is the default behavior of {@link Handles#invokeHandle(HandleInvoker)} and
     * {@link Handles#invokeVoidHandle(HandleInvoker.Void)}.</p>
     *
     * @return A wrapping {@link ThrowableWrapper}.
     *
     * @since 1.0.0
     */
    public static ThrowableWrapper rethrowAsRuntime() {
        return Wrap.INSTANCE;
    }

    /**
     * Obtains a {@link ThrowableWrapper} that unchecks exceptions.
     *
     * <p>Any {@link Throwable} instance is first unchecked using compiler tricks, and immediately rethrown. This makes
     * it impossible for other code to catch the exception given its checked nature. Care must be taken when using this
     * method.</p>
     *
     * <p>Differently from {@link #rethrowSneakily()}, this does not require access to the
     * {@link com.dwarveddonuts.neverwinter.unsafe} component, but does not prevent the verifier from knowing about the
     * exception being thrown. Also, differently from {@link #rethrowAsRuntime()}, no wrapping occurs.</p>
     *
     * @return An unchecking {@link ThrowableWrapper}.
     *
     * @since 1.0.0
     */
    public static ThrowableWrapper rethrowAsUnchecked() {
        return Uncheck.INSTANCE;
    }

    /**
     * Obtains a {@link ThrowableWrapper} that rethrows exceptions sneakily.
     *
     * <p>Any {@link Throwable} instance is rethrown immediately without letting the verifier nor the compiler know,
     * avoiding wrapping but also making it impossible for other code to catch the exception given its checked nature.
     * Care must be taken when using this method.</p>
     *
     * <p>Access to the {@link com.dwarveddonuts.neverwinter.unsafe} component is required to use this
     * {@code ThrowableWrapper}. For more information, refer to {@link TypedUnsafe#sneakyThrow(Throwable)}.</p>
     *
     * @return A sneaky {@link ThrowableWrapper}.
     *
     * @since 1.0.0
     */
    public static ThrowableWrapper rethrowSneakily() {
        return Sneak.INSTANCE;
    }

    abstract RuntimeException rethrow(final Throwable t);

    @Override
    public String toString() {
        return this.name;
    }
}
