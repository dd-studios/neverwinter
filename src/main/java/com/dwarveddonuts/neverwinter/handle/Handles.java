/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.handle;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.invoke.VarHandle;
import java.lang.invoke.WrongMethodTypeException;

/**
 * Holds various utility methods for easier and less verbose usage of the {@link java.lang.invoke} APIs.
 *
 * <p>In particular, the various methods available in this class allow for the creation of both {@link MethodHandle}s
 * and {@link VarHandle}s. Moreover, they also provide an interface for easier invocation that allows the client not to
 * worry about exception handling of the (very broad) {@link Throwable} that might be thrown by the invoked method.</p>
 *
 * <p>All methods in this class do not accept {@code null} parameters and never return {@code null}.</p>
 *
 * @since 1.0.0
 */
public final class Handles {
    private Handles() {}

    /**
     * Constructs a {@link MethodHandle} that invokes the desired method with the specified access mode.
     *
     * @param request The {@link RequestType} representing how the handle should be looked up.
     * @param access The {@link AccessType} indicating how the target method should be accessed.
     * @param owner The {@link Class} where the target method is declared in.
     * @param name The name of the target method.
     * @param returnType The {@link Class} representing the return type of the target method.
     * @param parameters The {@link Class}es representing the method's parameters, if any.
     * @return A {@link MethodHandle} that invokes the target method with the requested access.
     * @throws NullPointerException If any of the parameters is {@code null}.
     * @throws SecurityException If a security manager prevents access to the target handle.
     * @throws UnableToLinkHandleException If linkage of the {@link MethodHandle} fails because the method does not
     * exist or the given {@link AccessType} is prevented from accessing the target method due to constraints imposed by
     * the {@link RequestType} or the Java runtime.
     * @throws UnableToObtainLookupException If the {@link RequestType} cannot obtain the necessary
     * {@link MethodHandles.Lookup} instance to continue.
     * @throws UnsupportedOperationException If the given {@link AccessType} cannot be used to access the given method.
     *
     * @since 1.0.0
     */
    public static MethodHandle linkMethod(final RequestType request, final AccessType access, final Class<?> owner, final String name, final Class<?> returnType, final Class<?>... parameters) {
        return linkMethod(request, access, owner, name, MethodType.methodType(returnType, parameters));
    }

    /**
     * Constructs a {@link MethodHandle} that invokes the desired method with the specified access mode.
     *
     * @param request The {@link RequestType} representing how the handle should be looked up.
     * @param access The {@link AccessType} indicating how the target method should be accessed.
     * @param owner The {@link Class} where the target method is declared in.
     * @param name The name of the target method.
     * @param type The {@link MethodType} encoding return type and parameters of the method.
     * @return A {@link MethodHandle} that invokes the target method with the requested access.
     * @throws NullPointerException If any of the parameters is {@code null}.
     * @throws SecurityException If a security manager prevents access to the target handle.
     * @throws UnableToLinkHandleException If linkage of the {@link MethodHandle} fails because the method does not
     * exist or the given {@link AccessType} is prevented from accessing the target method due to constraints imposed by
     * the {@link RequestType} or the Java runtime.
     * @throws UnableToObtainLookupException If the {@link RequestType} cannot obtain the necessary
     * {@link MethodHandles.Lookup} instance to continue.
     * @throws UnsupportedOperationException If the given {@link AccessType} cannot be used to access the given method.
     *
     * @since 1.0.0
     */
    public static MethodHandle linkMethod(final RequestType request, final AccessType access, final Class<?> owner, final String name, final MethodType type) {
        final MethodHandles.Lookup lookup = request.lookupFor(owner);
        return access.access(lookup, owner, name, type);
    }

    /**
     * Constructs a {@link MethodHandle} that invokes the constructor of the given class.
     *
     * @param request The {@link RequestType} representing how the handle should be looked up.
     * @param owner The {@link Class} whose constructor should be obtained.
     * @param parameters The {@link Class}es representing the constructor parameters, if any.
     * @return A {@link MethodHandle} that invokes the target constructor.
     * @throws NullPointerException If any of the parameters is {@code null}.
     * @throws SecurityException If a security manager prevents access to the target handle.
     * @throws UnableToLinkHandleException If linkage of the {@link MethodHandle} fails because the method does not
     * exist or {@link AccessType#constructorAccess()} is prevented from accessing the target method due to constraints
     * imposed by the {@link RequestType} or the Java runtime.
     * @throws UnableToObtainLookupException If the {@link RequestType} cannot obtain the necessary
     * {@link MethodHandles.Lookup} instance to continue.
     *
     * @since 1.0.0
     */
    public static MethodHandle linkConstructor(final RequestType request, final Class<?> owner, final Class<?>... parameters) {
        return linkConstructor(request, owner, MethodType.methodType(void.class, parameters));
    }

    /**
     * Constructs a {@link MethodHandle} that invokes the constructor of the given class.
     *
     * @param request The {@link RequestType} representing how the handle should be looked up.
     * @param owner The {@link Class} whose constructor should be obtained.
     * @param type The {@link MethodType} instance representing the constructor's return type and arguments.
     * @return A {@link MethodHandle} that invokes the target constructor.
     * @throws NullPointerException If any of the parameters is {@code null}.
     * @throws SecurityException If a security manager prevents access to the target handle.
     * @throws UnableToLinkHandleException If linkage of the {@link MethodHandle} fails because the method does not
     * exist, the {@link MethodType}'s {@linkplain MethodType#returnType() return type} is not {@code void.class}, or
     * {@link AccessType#constructorAccess()} is prevented from accessing the target method due to constraints imposed
     * by the {@link RequestType} or the Java runtime.
     * @throws UnableToObtainLookupException If the {@link RequestType} cannot obtain the necessary
     * {@link MethodHandles.Lookup} instance to continue.
     *
     * @since 1.0.0
     */
    public static MethodHandle linkConstructor(final RequestType request, final Class<?> owner, final MethodType type) {
        return linkMethod(request, AccessType.constructorAccess(), owner, "<init>", type);
    }

    /**
     * Constructs a {@link VarHandle} that accesses the desired field with the specified access mode.
     *
     * @param request The {@link RequestType} representing how the handle should be looked up.
     * @param access The {@link AccessType} indicating how the target field should be accessed.
     * @param owner The {@link Class} where the target field is declared in.
     * @param name The name of the target field.
     * @param type The {@link Class} representing the type of the target field.
     * @return A {@link VarHandle} that accesses the given field.
     * @throws NullPointerException If any of the parameters is {@code null}.
     * @throws SecurityException If a security manager prevents access to the target handle.
     * @throws UnableToLinkHandleException If linkage of the {@link VarHandle} fails because the field does not exist or
     * the given {@link AccessType} is prevented from accessing the target method due to constraints imposed by the
     * {@link RequestType} or the Java runtime.
     * @throws UnableToObtainLookupException If the {@link RequestType} cannot obtain the necessary
     * {@link MethodHandles.Lookup} instance to continue.
     * @throws UnsupportedOperationException If the given {@link AccessType} cannot be used to access the given field.
     *
     * @since 1.0.0
     */
    public static VarHandle linkField(final RequestType request, final AccessType access, final Class<?> owner, final String name, final Class<?> type) {
        final MethodHandles.Lookup lookup = request.lookupFor(owner);
        return access.access(lookup, owner, name, type);
    }

    /**
     * Invokes a {@link MethodHandle} through the specified {@link HandleInvoker}.
     *
     * <p>Note that, due to constraints related to the polymorphic signature, it is not possible for this method to
     * invoke the target {@code MethodHandle} on its own. It is thus necessary for the caller to perform the call
     * itself through the {@code HandleInvoker}. This method will handle the invoker's execution and error catching.</p>
     *
     * <p>Any exceptions that might be thrown by the invocation will be caught and rethrown as children of
     * {@link RuntimeException} if needed.</p>
     *
     * <p>If the method that should be invoked is void, the caller should prefer the usage of
     * {@link #invokeVoidHandle(HandleInvoker.Void)} instead.</p>
     *
     * @param invoker The {@link HandleInvoker} responsible for the actual invocation of the handle.
     * @return The result of the handle invocation.
     * @param <T> The type returned by the handle's invocation.
     * @throws NullPointerException If {@code invoker} is {@code null}.
     * @throws HandleInvocationFailedException If the invocation of the handle failed due to a mismatch between the
     * {@link MethodHandle} and the invocation code.
     * @throws RuntimeException Or one of its subclasses, if the target method raises a {@link Throwable}. If the
     * exception is already a child of {@link RuntimeException} or is an {@link Error}, then it gets rethrown the same
     * way. Otherwise, the {@link Throwable} is wrapped in a {@link RuntimeException} and rethrown.
     *
     * @since 1.0.0
     */
    public static <T> T invokeHandle(final HandleInvoker<T> invoker) {
        return invokeHandle(ThrowableWrapper.rethrowAsRuntime(), invoker);
    }

    /**
     * Invokes a {@link MethodHandle} through the specified {@link HandleInvoker}, wrapping exceptions if needed.
     *
     * <p>Note that, due to constraints related to the polymorphic signature, it is not possible for this method to
     * invoke the target {@code MethodHandle} on its own. It is thus necessary for the caller to perform the call
     * itself through the {@code HandleInvoker}. This method will handle the invoker's execution and error catching.</p>
     *
     * <p>If the method that should be invoked is void, the caller should prefer the usage of
     * {@link #invokeVoidHandle(HandleInvoker.Void)} instead.</p>
     *
     * @param wrapper The {@link ThrowableWrapper} responsible for wrapping unchecked exceptions that might be thrown by
     *                the method invocation.
     * @param invoker The {@link HandleInvoker} responsible for the actual invocation of the handle.
     * @return The result of the handle invocation.
     * @param <T> The type returned by the handle's invocation.
     * @throws NullPointerException If any of the parameters is {@code null}.
     * @throws HandleInvocationFailedException If the invocation of the handle failed due to a mismatch between the
     * {@link MethodHandle} and the invocation code.
     * @throws RuntimeException Or one of its subclasses, if the target method raises a {@link Throwable}. If the
     * exception is already a child of {@link RuntimeException} or is an {@link Error}, then it gets rethrown the same
     * way. Otherwise, the {@link Throwable} is wrapped according to {@code wrapper}.
     *
     * @since 1.0.0
     */
    public static <T> T invokeHandle(final ThrowableWrapper wrapper, final HandleInvoker<T> invoker) {
        try {
            return invoker.invoke();
        } catch (final WrongMethodTypeException e) {
            throw HandleErrors.mismatch(e);
        } catch (final Throwable t) {
            if (t instanceof RuntimeException re) throw re;
            if (t instanceof Error e) throw e;
            throw wrapper.rethrow(t);
        }
    }

    /**
     * Invokes a {@code void} {@link MethodHandle} through the specified {@link HandleInvoker.Void}.
     *
     * <p>Note that, due to constraints related to the polymorphic signature, it is not possible for this method to
     * invoke the target {@code MethodHandle} on its own. It is thus necessary for the caller to perform the call
     * itself through the {@code HandleInvoker}. This method will handle the invoker's execution and error catching.</p>
     *
     * <p>Any exceptions that might be thrown by the invocation will be caught and rethrown as children of
     * {@link RuntimeException} if needed.</p>
     *
     * <p>Due to compiler constraints, it is suggested to use a method reference as the {@code invoker}, otherwise the
     * invocation might fail due to an assumed return type of {@link Object} that does not match reality.</p>
     *
     * @param invoker The {@link HandleInvoker.Void} responsible for the actual invocation of the handle.
     * @throws NullPointerException If {@code invoker} is {@code null}.
     * @throws HandleInvocationFailedException If the invocation of the handle failed due to a mismatch between the
     * {@link MethodHandle} and the invocation code.
     * @throws RuntimeException Or one of its subclasses, if the target method raises a {@link Throwable}. If the
     * exception is already a child of {@link RuntimeException} or is an {@link Error}, then it gets rethrown the same
     * way. Otherwise, the {@link Throwable} is wrapped in a {@link RuntimeException} and rethrown.
     *
     * @since 1.0.0
     */
    public static void invokeVoidHandle(final HandleInvoker.Void invoker) {
        invokeVoidHandle(ThrowableWrapper.rethrowAsRuntime(), invoker);
    }

    /**
     * Invokes a {@code void} {@link MethodHandle} through the specified {@link HandleInvoker.Void}.
     *
     * <p>Note that, due to constraints related to the polymorphic signature, it is not possible for this method to
     * invoke the target {@code MethodHandle} on its own. It is thus necessary for the caller to perform the call
     * itself through the {@code HandleInvoker}. This method will handle the invoker's execution and error catching.</p>
     *
     * <p>Due to compiler constraints, it is suggested to use a method reference as the {@code invoker}, otherwise the
     * invocation might fail due to an assumed return type of {@link Object} that does not match reality.</p>
     *
     * @param wrapper The {@link ThrowableWrapper} responsible for wrapping unchecked exceptions that might be thrown by
     *                the method invocation.
     * @param invoker The {@link HandleInvoker} responsible for the actual invocation of the handle.
     * @throws NullPointerException If any of the parameters is {@code null}.
     * @throws HandleInvocationFailedException If the invocation of the handle failed due to a mismatch between the
     * {@link MethodHandle} and the invocation code.
     * @throws RuntimeException Or one of its subclasses, if the target method raises a {@link Throwable}. If the
     * exception is already a child of {@link RuntimeException} or is an {@link Error}, then it gets rethrown the same
     * way. Otherwise, the {@link Throwable} is wrapped according to {@code wrapper}.
     *
     * @since 1.0.0
     */
    public static void invokeVoidHandle(final ThrowableWrapper wrapper, final HandleInvoker.Void invoker) {
        invokeHandle(wrapper, invoker);
    }
}
