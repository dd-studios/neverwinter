/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.handle;

import java.lang.invoke.MethodType;
import java.lang.invoke.WrongMethodTypeException;

final class HandleErrors {
    private HandleErrors() {}

    static RuntimeException noAccess(final AccessType access, final Class<?> receiver, final String name, final MethodType type, final IllegalAccessException e) {
        throw new UnableToLinkHandleException("Target method " + name(receiver, name, type) + " cannot be accessed with mode " + access, e);
    }

    static RuntimeException noAccess(final AccessType access, final Class<?> receiver, final String name, final Class<?> type, final IllegalAccessException e) {
        throw new UnableToLinkHandleException("Target field " + name(receiver, name, type) + " cannot be accessed with mode " + access, e);
    }

    static RuntimeException missing(final Class<?> receiver, final String name, final MethodType type, final NoSuchMethodException e) {
        throw new UnableToLinkHandleException("Target method " + name(receiver, name, type) + " does not exist", e);
    }

    static RuntimeException missing(final Class<?> receiver, final String name, final Class<?> type, final NoSuchFieldException e) {
        throw new UnableToLinkHandleException("Target field " + name(receiver, name, type) + " does not exist", e);
    }

    static RuntimeException unsupported(final AccessType access, final Class<?> receiver, final String name, final MethodType type) {
        throw new UnsupportedOperationException("Access type " + access + " cannot be used to access method " + name(receiver, name, type));
    }

    static RuntimeException unsupported(final AccessType access, final Class<?> receiver, final String name, final Class<?> type) {
        throw new UnsupportedOperationException("Access type " + access + " cannot be used to access field " + name(receiver, name, type));
    }

    static RuntimeException mismatch(final WrongMethodTypeException e) {
        throw new HandleInvocationFailedException("Unable to invoke the target method handle due to a method type mismatch", e);
    }

    private static String name(final Class<?> receiver, final String name, final MethodType type) {
        return name(receiver, name + type);
    }

    private static String name(final Class<?> receiver, final String name, final Class<?> type) {
        return name(receiver, name + " of type " + type.getName());
    }

    private static String name(final Class<?> receiver, final String member) {
        return receiver.getName() + '#' + member;
    }
}
