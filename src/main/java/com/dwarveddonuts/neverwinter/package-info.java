/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/**
 * <strong>NeverWinter</strong> is a library that allows easier and less verbose access to some low-level APIs of the
 * Java Programming Language.
 *
 * <p>The set of features provided by NeverWinter can be split into various components, each of which is fully contained
 * within a package. Some of them might require the functionality provided by others to work properly: such requirements
 * will be detailed in the package-specific documentation of the library.</p>
 *
 * <p>The various components provided currently are:</p>
 *
 * <ul>
 *     <li>
 *         <strong>{@link com.dwarveddonuts.neverwinter.unsafe} component</strong>: wraps the {@link sun.misc.Unsafe}
 *         class aiming to provide an easier set of APIs;
 *     </li>
 *     <li>
 *         <strong>{@link com.dwarveddonuts.neverwinter.handle} component</strong>: streamlines the usage of both the
 *         {@link java.lang.invoke.MethodHandle} and the {@link java.lang.invoke.VarHandle} APIs, while also unlocking
 *         additional capabilities;
 *     </li>
 *     <li>
 *         <strong>{@link com.dwarveddonuts.neverwinter.fs} component</strong>: provides various file-system related
 *         utilities for use cases that Java does not properly account for.
 *     </li>
 * </ul>
 *
 * @since 1.0.0
 */
package com.dwarveddonuts.neverwinter;
