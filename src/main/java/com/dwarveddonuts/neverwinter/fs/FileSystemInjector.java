/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.fs;

import com.dwarveddonuts.neverwinter.handle.AccessType;
import com.dwarveddonuts.neverwinter.handle.Handles;
import com.dwarveddonuts.neverwinter.handle.RequestType;

import java.lang.invoke.VarHandle;
import java.nio.file.spi.FileSystemProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Provides a set of utilities that allows users to inject custom {@link FileSystemProvider}s into the list of the ones
 * that Java will attempt to query.
 *
 * <p>This class performs unsafe operations and thus usage should be restricted at minimum. Moreover, the
 * {@code ServiceLoader} based approach should be preferred whenever possible through {@code provides} declarations in
 * {@code module-info}. This set of utilities is provided for cases where this behavior is insufficient or cannot be
 * used. Refer to {@link com.dwarveddonuts.neverwinter.fs} for more information.</p>
 *
 * <p>Usage of this class requires access to the {@link com.dwarveddonuts.neverwinter.unsafe} component.</p>
 *
 * @since 1.0.0
 */
public final class FileSystemInjector {
    private static final VarHandle LOCK = Handles.linkField(
            RequestType.trusted(),
            AccessType.staticAccess(),
            FileSystemProvider.class,
            "lock",
            Object.class
    );
    private static final VarHandle INSTALLED_PROVIDERS = Handles.linkField(
            RequestType.trusted(),
            AccessType.staticAccess(),
            FileSystemProvider.class,
            "installedProviders",
            List.class
    );

    private FileSystemInjector() {}

    /**
     * Injects the given {@link FileSystemProvider} into the list of available ones.
     *
     * <p>If a provider for the same {@linkplain FileSystemProvider#getScheme() scheme} already exists, then the
     * addition is skipped, as Java would not be able to distinguish between the two providers when queried with a
     * {@link java.net.URI}.</p>
     *
     * <p>Otherwise, the provider is injected and the method returns normally.</p>
     *
     * <p>Calling this method is the same as invoking {@link #injectFileSystem(FileSystemProvider, int)} with the same
     * {@code provider} as the first argument and asking it to perform a single attempt.</p>
     *
     * @param provider The {@link FileSystemProvider} that should be injected.
     * @throws NullPointerException If the given {@link FileSystemProvider} is {@code null}.
     * @throws UnableToInjectFileSystemException If the injection of the provider failed.
     *
     * @since 1.0.0
     */
    public static void injectFileSystem(final FileSystemProvider provider) {
        injectFileSystem(provider, 1);
    }

    /**
     * Injects the given {@link FileSystemProvider} into the list of available ones.
     *
     * <p>If a provider for the same {@linkplain FileSystemProvider#getScheme() scheme} already exists, then the
     * addition is skipped, as Java would not be able to distinguish between the two providers when queried with a
     * {@link java.net.URI}.</p>
     *
     * <p>Otherwise, the provider is injected and the method returns normally.</p>
     *
     * <p>This method attempts injection of the provider {@code tries} times, to account for potentially unsafe
     * concurrent access by other actors that might be doing the same thing. If the value given for {@code tries} is
     * {@code 0} or lower, then the no attempts are performed and an exception is raised immediately.</p>
     *
     * @param provider The {@link FileSystemProvider} that should be injected.
     * @param tries The amount of times that the injection should be tried before an exception is raised.
     * @throws NullPointerException If the given {@link FileSystemProvider} is {@code null}.
     * @throws UnableToInjectFileSystemException If the injection of the provider failed every time.
     *
     * @since 1.0.0
     */
    public static void injectFileSystem(final FileSystemProvider provider, final int tries) {
        if (exists(provider)) {
            return;
        }

        int counter = tries;
        while (counter-- > 0) {
            synchronized (LOCK.get()) {
                // Let's do another check just in case another instance of this class is running on another thread, and
                // we happened to perform the query prior to the lock acquisition.
                if (exists(provider)) {
                    return;
                }

                @SuppressWarnings("unchecked")
                final List<FileSystemProvider> providers = (List<FileSystemProvider>) (List<?>) INSTALLED_PROVIDERS.getVolatile();

                final List<FileSystemProvider> newProviders = new ArrayList<>(providers);
                newProviders.add(provider);

                if (INSTALLED_PROVIDERS.compareAndSet(providers, Collections.unmodifiableList(newProviders))) {
                    // Make sure that the variable has been altered correctly, just in case some other thread is not
                    // using the lock correctly or some other JVM shenanigans are going on
                    if (exists(provider)) {
                        return;
                    }
                }
            }
        }

        throw new UnableToInjectFileSystemException(provider);
    }

    private static boolean exists(final FileSystemProvider provider) {
        final String scheme = provider.getScheme();
        return FileSystemProvider.installedProviders().stream().anyMatch(it -> Objects.equals(it.getScheme(), scheme));
    }
}
