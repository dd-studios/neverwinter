/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.unsafe;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.util.function.Supplier;

/**
 * Wraps the {@link sun.misc.Unsafe} class by providing simpler, {@code static}-based access to the various methods
 * available in it.
 *
 * <p>The client should note that while the API mirrors what is provided in {@code Unsafe}, some of the method names
 * have been altered to either follow the conventions dictated now by {@code jdk.internal.misc.Unsafe} or more commonly
 * used names (such as {@code malloc} instead of {@code allocateMemory}).</p>
 *
 * <p>The same warnings that are provided in {@code Unsafe}'s documentation regarding safety features apply.</p>
 *
 * @since 1.0.0
 */
public final class Unsafe {
    private static final sun.misc.Unsafe UNSAFE = ((Supplier<sun.misc.Unsafe>) () -> {
        try {
            final Class<sun.misc.Unsafe> clazz = sun.misc.Unsafe.class;
            final Field field = clazz.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            return clazz.cast(field.get(null));
        } catch (final ReflectiveOperationException e) {
            throw new IllegalStateException("Unable to obtain Unsafe instance", e);
        }
    }).get();

    /**
     * Marks an offset as being invalid.
     *
     * <p>This constant is returned by {@link #staticFieldOffset(Field)}, {@link #objectFieldOffset(Field)}, or
     * {@link #arrayBaseOffset(Class)} when invalid parameters are given or when no suitable offset is found.</p>
     *
     * @since 1.0.0
     */
    public static final int INVALID_FIELD_OFFSET = sun.misc.Unsafe.INVALID_FIELD_OFFSET;

    /**
     * Holds the size used to represent a memory address.
     *
     * <p>In other words, this number corresponds to the size of a pointer on the target machine, also represented by
     * {@code size_t} in native code. It is guaranteed to be either {@code 4}, for 32-bit architectures, or {@code 8},
     * for 64-bit architectures.</p>
     *
     * <p>The value of this field is the same one as returned by {@link #addressSize()}.</p>
     *
     * @since 1.0.0
     */
    public static final int ADDRESS_SIZE = addressSize();
    /**
     * Holds the size of a native memory page.
     *
     * <p>Differently from {@link #ADDRESS_SIZE}, no guarantees other than it being a power of two are made on this
     * value.</p>
     *
     * <p>The value of this field is the same one as returned by {@link #pageSize()}.</p>
     *
     * @since 1.0.0
     */
    public static final int PAGE_SIZE = pageSize();

    /**
     * Holds the value of the base offset of a {@code boolean} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code boolean[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_BOOLEAN_BASE_OFFSET = sun.misc.Unsafe.ARRAY_BOOLEAN_BASE_OFFSET;
    /**
     * Holds the value of the base offset of a {@code byte} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code byte[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_BYTE_BASE_OFFSET = sun.misc.Unsafe.ARRAY_BYTE_BASE_OFFSET;
    /**
     * Holds the value of the base offset of a {@code char} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code char[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_CHAR_BASE_OFFSET = sun.misc.Unsafe.ARRAY_CHAR_BASE_OFFSET;
    /**
     * Holds the value of the base offset of a {@code double} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code double[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_DOUBLE_BASE_OFFSET = sun.misc.Unsafe.ARRAY_DOUBLE_BASE_OFFSET;
    /**
     * Holds the value of the base offset of a {@code float} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code float[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_FLOAT_BASE_OFFSET = sun.misc.Unsafe.ARRAY_FLOAT_BASE_OFFSET;
    /**
     * Holds the value of the base offset of an {@code int} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code int[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_INT_BASE_OFFSET = sun.misc.Unsafe.ARRAY_INT_BASE_OFFSET;
    /**
     * Holds the value of the base offset of a {@code long} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code long[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_LONG_BASE_OFFSET = sun.misc.Unsafe.ARRAY_LONG_BASE_OFFSET;
    /**
     * Holds the value of the base offset of an {@link Object} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code Object[].class}
     * as an argument.</p>
     *
     * <p>Note that this value is only valid for the {@code Object} base class: arrays of other reference types might or
     * might not have this same base offset. Clients are thus encouraged to use the value returned by the method in such
     * cases at all times.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_OBJECT_BASE_OFFSET = sun.misc.Unsafe.ARRAY_OBJECT_BASE_OFFSET;
    /**
     * Holds the value of the base offset of a {@code short} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayBaseOffset(Class)} with {@code short[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_SHORT_BASE_OFFSET = sun.misc.Unsafe.ARRAY_SHORT_BASE_OFFSET;

    /**
     * Holds the value of the index scaling used by a {@code boolean} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code boolean[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_BOOLEAN_INDEX_SCALE = sun.misc.Unsafe.ARRAY_BOOLEAN_INDEX_SCALE;
    /**
     * Holds the value of the index scaling used by a {@code byte} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code byte[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_BYTE_INDEX_SCALE = sun.misc.Unsafe.ARRAY_BYTE_INDEX_SCALE;
    /**
     * Holds the value of the index scaling used by a {@code char} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code char[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_CHAR_INDEX_SCALE = sun.misc.Unsafe.ARRAY_CHAR_INDEX_SCALE;
    /**
     * Holds the value of the index scaling used by a {@code double} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code double[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_DOUBLE_INDEX_SCALE = sun.misc.Unsafe.ARRAY_DOUBLE_INDEX_SCALE;
    /**
     * Holds the value of the index scaling used by a {@code float} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code float[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_FLOAT_INDEX_SCALE = sun.misc.Unsafe.ARRAY_FLOAT_INDEX_SCALE;
    /**
     * Holds the value of the index scaling used by an {@code int} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code int[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_INT_INDEX_SCALE = sun.misc.Unsafe.ARRAY_INT_INDEX_SCALE;
    /**
     * Holds the value of the index scaling used by a {@code long} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code long[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_LONG_INDEX_SCALE = sun.misc.Unsafe.ARRAY_LONG_INDEX_SCALE;
    /**
     * Holds the value of the index scaling used by an {@link Object} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code Object[].class}
     * as an argument.</p>
     *
     * <p>Note that this value is only valid for the {@code Object} base class: arrays of other reference types might or
     * might not have this same index scale. Clients are thus encouraged to use the value returned by the method in such
     * cases at all times.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_OBJECT_INDEX_SCALE = sun.misc.Unsafe.ARRAY_OBJECT_INDEX_SCALE;
    /**
     * Holds the value of the index scaling used by a {@code short} array.
     *
     * <p>The value corresponds to the result of calling {@link #arrayIndexScale(Class)} with {@code short[].class}
     * as an argument.</p>
     *
     * @since 1.0.0
     */
    public static final int ARRAY_SHORT_INDEX_SCALE = sun.misc.Unsafe.ARRAY_SHORT_INDEX_SCALE;

    private Unsafe() {}

    /**
     * Obtains the {@code boolean} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code boolean}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code boolean} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code boolean[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_BOOLEAN_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code boolean[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_BOOLEAN_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getByte(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code boolean}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static boolean getBoolean(final Object o, final long offset) {
        return UNSAFE.getBoolean(o, offset);
    }

    /**
     * Obtains the {@code byte} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code byte}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code byte} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code byte[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_BYTE_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code byte[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_BYTE_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getByte(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code byte}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static byte getByte(final Object o, final long offset) {
        return UNSAFE.getByte(o, offset);
    }

    /**
     * Obtains the {@code char} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code char}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code char} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code char[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_CHAR_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code char[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_CHAR_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getChar(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code char}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static char getChar(final Object o, final long offset) {
        return UNSAFE.getChar(o, offset);
    }

    /**
     * Obtains the {@code double} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code double}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code double} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code double[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_DOUBLE_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code double[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_DOUBLE_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getDouble(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code double}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static double getDouble(final Object o, final long offset) {
        return UNSAFE.getDouble(o, offset);
    }

    /**
     * Obtains the {@code float} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code float}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code float} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code float[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_FLOAT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code float[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_FLOAT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getFloat(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code float}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static float getFloat(final Object o, final long offset) {
        return UNSAFE.getFloat(o, offset);
    }

    /**
     * Obtains the {@code int} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code int}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an {@code int} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code int[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_INT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code int[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_INT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getInt(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code int}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static int getInt(final Object o, final long offset) {
        return UNSAFE.getInt(o, offset);
    }

    /**
     * Obtains the {@code long} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code long}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code long} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code long[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_LONG_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code long[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_LONG_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getLong(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code long}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static long getLong(final Object o, final long offset) {
        return UNSAFE.getLong(o, offset);
    }

    /**
     * Obtains the reference value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually a reference.
     * Note that no additional assumptions are made on the type of the object: it is up to client code to cast it back
     * to the correct type, if required. In any other case, the result is undefined. Take note that wrong arguments
     * might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an array of a certain {@link Class} {@code C}, and {@code offset} is a value that was
     *         obtained through {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code C} to
     *         {@link #arrayBaseOffset(Class)}, and {@code S} is the index scale of the array obtained by supplying
     *         {@code C} to {@link #arrayIndexScale(Class)}.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is unavailable, as native variables
     * cannot be properly represented via actual Java types. If such a mode is needed, it is suggested to use
     * {@link #getAddress(long)} and wrap the resulting pointer into a Java object.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is a reference type; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static Object getReference(final Object o, final long offset) {
        return UNSAFE.getObject(o, offset);
    }

    /**
     * Obtains the {@code short} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code short}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code short} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code short[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_SHORT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code short[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_SHORT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getShort(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code short}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static short getShort(final Object o, final long offset) {
        return UNSAFE.getShort(o, offset);
    }

    /**
     * Sets the {@code boolean} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code boolean}. Therefore, the value can be properly set if and only
     * if both {@code o} and {@code offset} have been obtained according to the rules outlined in the following
     * paragraph and the field or array element pointed by them is actually of type {@code boolean}. In any other case,
     * the behavior is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code boolean} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code boolean[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_BOOLEAN_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code boolean[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_BOOLEAN_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putByte(long, byte)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putBoolean(final Object o, final long offset, final boolean x) {
        UNSAFE.putBoolean(o, offset, x);
    }

    /**
     * Sets the {@code byte} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code byte}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code byte}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code byte} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code byte[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_BYTE_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code byte[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_BYTE_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putByte(long, byte)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putByte(final Object o, final long offset, final byte x) {
        UNSAFE.putByte(o, offset, x);
    }

    /**
     * Sets the {@code char} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code char}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code char}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code char} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code char[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_CHAR_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code char[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_CHAR_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putChar(long, char)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putChar(final Object o, final long offset, final char x) {
        UNSAFE.putChar(o, offset, x);
    }

    /**
     * Sets the {@code double} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code double}. Therefore, the value can be properly set if and only
     * if both {@code o} and {@code offset} have been obtained according to the rules outlined in the following
     * paragraph and the field or array element pointed by them is actually of type {@code double}. In any other case,
     * the behavior is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code double} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code double[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_DOUBLE_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code double[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_DOUBLE_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putDouble(long, double)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putDouble(final Object o, final long offset, final double x) {
        UNSAFE.putDouble(o, offset, x);
    }

    /**
     * Sets the {@code float} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code float}. Therefore, the value can be properly set if and only
     * if both {@code o} and {@code offset} have been obtained according to the rules outlined in the following
     * paragraph and the field or array element pointed by them is actually of type {@code float}. In any other case,
     * the behavior is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code float} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code float[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_FLOAT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code float[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_FLOAT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putFloat(long, float)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putFloat(final Object o, final long offset, final float x) {
        UNSAFE.putFloat(o, offset, x);
    }

    /**
     * Sets the {@code int} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code int}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code int}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an {@code int} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code int[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_INT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code int[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_INT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putInt(long, int)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putInt(final Object o, final long offset, final int x) {
        UNSAFE.putInt(o, offset, x);
    }

    /**
     * Sets the {@code long} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code long}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code long}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code long} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code long[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_LONG_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code long[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_LONG_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putLong(long, long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putLong(final Object o, final long offset, final long x) {
        UNSAFE.putLong(o, offset, x);
    }

    /**
     * Sets the reference value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually contains a reference, nor that it has the same type as the runtime type of
     * {@code x}. Therefore, the value can be properly set if and only if both {@code o} and {@code offset} have been
     * obtained according to the rules outlined in the following paragraph and the field or array element pointed by
     * them can actually hold a reference with the same type of {@code x}, meaning that {@code x.getClass()} is the same
     * or a subclass of the field or array element. In any other case, the behavior is undefined. Take note that wrong
     * arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an array of a certain {@link Class} {@code C}, and {@code offset} is a value that was
     *         obtained through {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code C} to
     *         {@link #arrayBaseOffset(Class)}, and {@code S} is the index scale of the array obtained by supplying
     *         {@code C} to {@link #arrayIndexScale(Class)}.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is unavailable, as native variables
     * cannot be properly represented via actual Java types. If such a mode is needed, it is suggested to unwrap the
     * Java object into a native address and then use the resulting {@code long} in {@link #putAddress(long, long)}.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putReference(final Object o, final long offset, final Object x) {
        UNSAFE.putObject(o, offset, x);
    }

    /**
     * Sets the {@code short} value of the field or array element at the given location.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code short}. Therefore, the value can be properly set if and only
     * if both {@code o} and {@code offset} have been obtained according to the rules outlined in the following
     * paragraph and the field or array element pointed by them is actually of type {@code short}. In any other case,
     * the behavior is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code short} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code short[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_SHORT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code short[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_SHORT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putShort(long, short)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putShort(final Object o, final long offset, final short x) {
        UNSAFE.putShort(o, offset, x);
    }



    /**
     * Obtains the pointer stored at the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the given {@code address} points into a memory
     * region that was allocated through {@link #malloc(long)}. In any other case, the result is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the type
     * stored at the given address, which is assumed to be a pointer.</p>
     *
     * <p>The obtained value is guaranteed to be a {@code long}. If pointers on a specific JVM implementation are less
     * than 64-bits wide, then the result is treated as an unsigned long and extended to be 64-bits in size. Clients
     * that want to verify the actual size of the pointer can refer to either {@link #addressSize()} or
     * {@link #ADDRESS_SIZE} for the size in bytes.</p>
     *
     * <p>The pointer behaves exactly as any other pointer in native code, meaning that pointer arithmetic is defined. A
     * client that wishes to offset the pointer by a given amount of bytes can simply do so by adding that number to the
     * result of this method.</p>
     *
     * @param address The address at which the pointer is stored.
     * @return The pointer stored at the given address.
     *
     * @since 1.0.0
     */
    public static long getAddress(final long address) {
        return UNSAFE.getAddress(address);
    }

    /**
     * Obtains the {@code byte} stored at the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the given {@code address} points into a memory
     * region that was allocated through {@link #malloc(long)}. In any other case, the result is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the type
     * stored at the given address, which is assumed to be compatible with a {@code byte}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #getByte(Object, long)} is <strong>discouraged</strong>, as Java variables
     * need not have the same memory layouts as native variables nor this fact can be considered portable across
     * multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code byte} is stored.
     * @return The {@code byte} stored at the given address.
     *
     * @since 1.0.0
     */
    public static byte getByte(final long address) {
        return UNSAFE.getByte(address);
    }

    /**
     * Obtains the {@code char} stored at the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the given {@code address} points into a memory
     * region that was allocated through {@link #malloc(long)}. In any other case, the result is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the type
     * stored at the given address, which is assumed to be compatible with a {@code char}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #getChar(Object, long)} is <strong>discouraged</strong>, as Java variables
     * need not have the same memory layouts as native variables nor this fact can be considered portable across
     * multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code char} is stored.
     * @return The {@code char} stored at the given address.
     *
     * @since 1.0.0
     */
    public static char getChar(final long address) {
        return UNSAFE.getChar(address);
    }

    /**
     * Obtains the {@code double} stored at the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the given {@code address} points into a memory
     * region that was allocated through {@link #malloc(long)}. In any other case, the result is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the type
     * stored at the given address, which is assumed to be compatible with a {@code double}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #getDouble(Object, long)} is <strong>discouraged</strong>, as Java variables
     * need not have the same memory layouts as native variables nor this fact can be considered portable across
     * multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code double} is stored.
     * @return The {@code double} stored at the given address.
     *
     * @since 1.0.0
     */
    public static double getDouble(final long address) {
        return UNSAFE.getDouble(address);
    }

    /**
     * Obtains the {@code float} stored at the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the given {@code address} points into a memory
     * region that was allocated through {@link #malloc(long)}. In any other case, the result is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the type
     * stored at the given address, which is assumed to be compatible with a {@code float}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #getFloat(Object, long)} is <strong>discouraged</strong>, as Java variables
     * need not have the same memory layouts as native variables nor this fact can be considered portable across
     * multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code float} is stored.
     * @return The {@code float} stored at the given address.
     *
     * @since 1.0.0
     */
    public static float getFloat(final long address) {
        return UNSAFE.getFloat(address);
    }

    /**
     * Obtains the {@code int} stored at the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the given {@code address} points into a memory
     * region that was allocated through {@link #malloc(long)}. In any other case, the result is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the type
     * stored at the given address, which is assumed to be compatible with an {@code int}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #getInt(Object, long)} is <strong>discouraged</strong>, as Java variables
     * need not have the same memory layouts as native variables nor this fact can be considered portable across
     * multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code int} is stored.
     * @return The {@code int} stored at the given address.
     *
     * @since 1.0.0
     */
    public static int getInt(final long address) {
        return UNSAFE.getInt(address);
    }

    /**
     * Obtains the {@code long} stored at the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the given {@code address} points into a memory
     * region that was allocated through {@link #malloc(long)}. In any other case, the result is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the type
     * stored at the given address, which is assumed to be compatible with a {@code long}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #getLong(Object, long)} is <strong>discouraged</strong>, as Java variables
     * need not have the same memory layouts as native variables nor this fact can be considered portable across
     * multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code long} is stored.
     * @return The {@code long} stored at the given address.
     *
     * @since 1.0.0
     */
    public static long getLong(final long address) {
        return UNSAFE.getLong(address);
    }

    /**
     * Obtains the {@code short} stored at the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the given {@code address} points into a memory
     * region that was allocated through {@link #malloc(long)}. In any other case, the result is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the type
     * stored at the given address, which is assumed to be compatible with a {@code short}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #getShort(Object, long)} is <strong>discouraged</strong>, as Java variables
     * need not have the same memory layouts as native variables nor this fact can be considered portable across
     * multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code short} is stored.
     * @return The {@code short} stored at the given address.
     *
     * @since 1.0.0
     */
    public static short getShort(final long address) {
        return UNSAFE.getShort(address);
    }

    /**
     * Stores the given pointer to the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>This method will properly set the value if and only if the given {@code address} points into a memory region
     * that was allocated through {@link #malloc(long)}. In any other case, the actual behavior is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the actual
     * type that should be stored at the given address, which is assumed to be a pointer.</p>
     *
     * <p>If pointers on a specific JVM implementation are less than 64-bits wide, then the result is truncated to the
     * correct size before being written, with the upper-most exceeding bits being discarded. Clients that want to
     * verify the actual size of the pointer written can refer to either {@link #addressSize()} or {@link #ADDRESS_SIZE}
     * for the size in bytes.</p>
     *
     * @param address The address at which the pointer should be stored.
     * @param x The value to write at the given address.
     *
     * @since 1.0.0
     */
    public static void putAddress(final long address, final long x) {
        UNSAFE.putAddress(address, x);
    }

    /**
     * Stores the given {@code byte} to the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>This method will properly set the value if and only if the given {@code address} points into a memory region
     * that was allocated through {@link #malloc(long)}. In any other case, the actual behavior is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the actual
     * type that should be stored at the given address, which is assumed to be a {@code byte}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #putByte(Object, long, byte)} is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code byte} should be stored.
     * @param x The value to write at the given address.
     *
     * @since 1.0.0
     */
    public static void putByte(final long address, final byte x) {
        UNSAFE.putByte(address, x);
    }

    /**
     * Stores the given {@code char} to the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>This method will properly set the value if and only if the given {@code address} points into a memory region
     * that was allocated through {@link #malloc(long)}. In any other case, the actual behavior is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the actual
     * type that should be stored at the given address, which is assumed to be a {@code char}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #putChar(Object, long, char)} is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code char} should be stored.
     * @param x The value to write at the given address.
     *
     * @since 1.0.0
     */
    public static void putChar(final long address, final char x) {
        UNSAFE.putChar(address, x);
    }

    /**
     * Stores the given {@code float} to the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>This method will properly set the value if and only if the given {@code address} points into a memory region
     * that was allocated through {@link #malloc(long)}. In any other case, the actual behavior is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the actual
     * type that should be stored at the given address, which is assumed to be a {@code float}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #putFloat(Object, long, float)} is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code float} should be stored.
     * @param x The value to write at the given address.
     *
     * @since 1.0.0
     */
    public static void putFloat(final long address, final float x) {
        UNSAFE.putFloat(address, x);
    }

    /**
     * Stores the given {@code double} to the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>This method will properly set the value if and only if the given {@code address} points into a memory region
     * that was allocated through {@link #malloc(long)}. In any other case, the actual behavior is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the actual
     * type that should be stored at the given address, which is assumed to be a {@code double}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #putDouble(Object, long, double)} is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code double} should be stored.
     * @param x The value to write at the given address.
     *
     * @since 1.0.0
     */
    public static void putDouble(final long address, final double x) {
        UNSAFE.putDouble(address, x);
    }

    /**
     * Stores the given {@code int} to the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>This method will properly set the value if and only if the given {@code address} points into a memory region
     * that was allocated through {@link #malloc(long)}. In any other case, the actual behavior is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the actual
     * type that should be stored at the given address, which is assumed to be an {@code int}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #putInt(Object, long, int)} is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code int} should be stored.
     * @param x The value to write at the given address.
     *
     * @since 1.0.0
     */
    public static void putInt(final long address, final int x) {
        UNSAFE.putInt(address, x);
    }

    /**
     * Stores the given {@code long} to the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>This method will properly set the value if and only if the given {@code address} points into a memory region
     * that was allocated through {@link #malloc(long)}. In any other case, the actual behavior is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the actual
     * type that should be stored at the given address, which is assumed to be a {@code long}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #putLong(Object, long, long)} is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code long} should be stored.
     * @param x The value to write at the given address.
     *
     * @since 1.0.0
     */
    public static void putLong(final long address, final long x) {
        UNSAFE.putLong(address, x);
    }

    /**
     * Stores the given {@code short} to the given memory location.
     *
     * <p>The location of the element is determined through a <em>single-register</em>-like addressing mode, with the
     * {@code address} being used to uniquely identify the address in JVM memory.</p>
     *
     * <p>This method will properly set the value if and only if the given {@code address} points into a memory region
     * that was allocated through {@link #malloc(long)}. In any other case, the actual behavior is undefined. Take note
     * that wrong arguments might lead to crashes in native code. Moreover, no verifications are performed on the actual
     * type that should be stored at the given address, which is assumed to be a {@code short}.</p>
     *
     * <p>Usage of this method to access fields and array elements instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #putShort(Object, long, short)} is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * @param address The address at which the {@code short} should be stored.
     * @param x The value to write at the given address.
     *
     * @since 1.0.0
     */
    public static void putShort(final long address, final short x) {
        UNSAFE.putShort(address, x);
    }



    /**
     * Allocates a native memory region of the given size.
     *
     * <p>The memory region that will be allocated will be aligned for all value types, meaning that the size specified
     * might be rounded up (<em>never</em> down) to account for proper alignment.</p>
     *
     * <p>The contents of the allocated memory region are not cleared out and thus any attempts to read into areas that
     * have not been written before will likely result in garbage data. Clients that would like to initialize memory to
     * a specific value should invoke the {@link #memset(long, long, byte)} method after allocation.</p>
     *
     * <p>It is the caller's responsibility to free the memory region once it has no more use for it through the
     * {@link #free(long)} method. No garbage collection mechanisms will be allowed to modify or reclaim the given area
     * of memory.</p>
     *
     * <p>The memory region allocated has a static size, namely the one specified by {@code bytes}. Callers that wish to
     * alter the allocated memory area may do so with {@link #realloc(long, long)}.</p>
     *
     * @param bytes The amount of bytes that need to be allocated. It must be a positive number.
     * @return A pointer to the first byte of the allocated memory region; it is always guaranteed not to be a null
     * pointer (i.e., it will never be {@code 0}).
     * @throws OutOfMemoryError If the allocation was refused because of size constraints.
     * @throws RuntimeException <strong>Optionally</strong>, if the given memory size is invalid either because it is
     * negative or is of a type unrepresentable by the native platform (e.g., trying to allocate more than 3 GBs on a
     * 32-bit platform). Note that this exception might not be thrown even if these conditions are met due to aggressive
     * JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    public static long malloc(final long bytes) {
        return UNSAFE.allocateMemory(bytes);
    }

    /**
     * Resizes an already existing memory region to the given size.
     *
     * <p>The resized memory region will still be aligned for all value types, meaning that the size specified might be
     * rounded up (<em>never</em> down) to account for proper alignment.</p>
     *
     * <p>The behavior of this method changes depending on the values given to both {@code address} and {@code bytes}
     * as determined by the following rules:</p>
     *
     * <ul>
     *     <li>
     *         If {@code address} is not a null pointer and {@code bytes} is not zero, then the specified memory region
     *         is resized to the given size if possible. If the size is increased, then the "added" space is not cleared
     *         out and will likely contain garbage data; it is possible for a caller to use
     *         {@link #memset(long, long, byte)} to manually clear the region if needed. On the other hand, if the size
     *         is decreased, the "excluded" space is automatically reclaimed. It is then the caller's responsibility to
     *         manually free the memory region through {@link #free(long)} once it has exhausted its purpose, or to
     *         alter the size of the region again with this same method.
     *     </li>
     *     <li>
     *         If {@code address} is a null pointer and {@code bytes} is not zero, then this method behaves exactly as
     *         {@link #malloc(long)} as if it were called with {@code bytes} as its only argument.
     *     </li>
     *     <li>
     *         If {@code address} is not a null pointer and {@code bytes} is zero, then this method behaves exactly as
     *         {@link #free(long)} as if it were called with {@code address} as its only argument.
     *     </li>
     *     <li>
     *         If {@code address} is a null pointer and {@code bytes} is zero, then this method does nothing.
     *     </li>
     * </ul>
     *
     * @param address A pointer into the memory region that should be resized, or a null pointer.
     * @param bytes The new size of the memory region. It must be a positive number or zero to free the given region.
     * @return A pointer to the new memory region, or a null pointer if the region was freed, according to the rules
     * outlined before. Note that clients may not assume that {@code address} and the return value coincide, as no such
     * guarantees are provided by the runtime: once this method has been called, clients should only refer to the memory
     * region through the pointer provided by this method.
     * @throws OutOfMemoryError If the reallocation was refused because of size constraints.
     * @throws RuntimeException <strong>Optionally</strong>, if the given memory size is invalid either because it is
     * negative or is of a type unrepresentable by the native platform (e.g. trying to reallocate a memory region to be
     * of more than 3 GBs in size on a 32-bit platform). Note that this exception might not be thrown even if these
     * conditions are met due to aggressive JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    public static long realloc(final long address, final long bytes) {
        return UNSAFE.reallocateMemory(address, bytes);
    }

    /**
     * Sets all bytes in a given memory region to the given value.
     *
     * <p>The location of the memory region is determined through a <em>double-register</em>-like addressing mode, where
     * the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in JVM memory.</p>
     *
     * <p>This method will properly set the values of the memory region if and only if the values of {@code o},
     * {@code offset} and {@code bytes} have been obtained according to the rules detailed in the following paragraph.
     * If that is not the case, this might lead to crashes in native code.</p>
     *
     * <p>The rules that should be followed by {@code o}, {@code offset}, and {@code bytes} are:</p>
     *
     * <ul>
     *     <li>
     *         {@code o} is a primitive array of type {@code C}, whereas {@code offset} is a value that was obtained
     *         through {@code B + I * S}, where {@code I} is a valid index into the array, {@code B} is the base offset
     *         of the array obtained by supplying {@code C} to {@link #arrayBaseOffset(Class)}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code C} to {@link #arrayIndexScale(Class)}, and
     *         {@code bytes} is an amount that identifies a memory region that is bounded between {@code offset} and
     *         the array's length;
     *     </li>
     *     <li>
     *         {@code o} is {@code null}, {@code offset} is an absolute address that can be supplied to
     *         {@link #memset(long, long, byte)}, and {@code bytes} is any positive number, in which case this method
     *         will act as if it were invoked in a <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The memory stores are executed in atomic units whose size is determined by both the {@code bytes} parameter
     * and the actual address that is obtained from the <em>double-register</em>-like addressing mode, which will be
     * identified by {@code address} in the following text. In practice, the JVM will verify if {@code bytes} and
     * {@code address} are the same number modulo {@code i} (where {@code i} assumes in order the values {@code 8},
     * {@code 4}, {@code 2}, and {@code 1}). The first value of {@code i} that satisfies the equality will determine the
     * size of the atomic blocks. For example, if {@code address mod 8 == bytes mod 8}, then bytes of size {@code 8}
     * will be set atomically.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param bytes The length of the memory region to set. It must be a non-negative number.
     * @param value The value to which every byte of the region should be set to.
     * @throws RuntimeException <strong>Optionally</strong>, if the given arguments do not respect the conditions
     * detailed above or if {@code bytes} is a negative number. Note that this exception might not be thrown even if
     * these conditions are met due to aggressive JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static void memset(final Object o, final long offset, final long bytes, final byte value) {
        UNSAFE.setMemory(o, offset, bytes, value);
    }

    /**
     * Sets all bytes in a given memory region to the given value.
     *
     * <p>The location of the memory region is determined through a <em>single-register</em>-like addressing mode, where
     * the {@code address} is used as an absolute address into JVM memory.</p>
     *
     * <p>Usage of this method to set elements of primitive arrays instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #memset(Object, long, long, byte)} is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The memory stores are executed in atomic units whose size is determined by both the {@code bytes} parameter
     * and {@code address}. In practice, the JVM will verify if {@code bytes} and {@code address} are the same number
     * modulo {@code i} (where {@code i} assumes in order the values {@code 8}, {@code 4}, {@code 2}, and {@code 1}).
     * The first value of {@code i} that satisfies the equality will determine the size of the atomic blocks. For
     * example, if {@code address mod 8 == bytes mod 8}, then bytes of size {@code 8} will be set atomically.</p>
     *
     * @param address The address that marks the beginning of the memory region that should be set.
     * @param bytes The length of the memory region to set. It must be non-negative.
     * @param value The value to which every byte of the region should be set to.
     * @throws RuntimeException <strong>Optionally</strong>, if the value of {@code bytes} is negative. Note that this
     * exception might not be thrown even if these conditions are met due to aggressive JVM optimizations that might be
     * performed.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static void memset(final long address, final long bytes, final byte value) {
        UNSAFE.setMemory(address, bytes, value);
    }

    /**
     * Copies all bytes from the given source memory region to the given destination.
     *
     * <p>The location of both memory regions is determined through a <em>double-register</em>-like addressing mode,
     * where the {@link Object} and the corresponding {@code long} are used to determine the actual address in JVM
     * memory. The checked pairs are {@code srcBase} together with {@code srcOffset} and {@code destBase} together with
     * {@code destOffset}.</p>
     *
     * <p>This method will properly copy the values across the two regions if and only if the values of both
     * {@code Object}-{@code long} pairs and the values of {@code bytes} follow the rules detailed in the following
     * paragraph. If that is not the case, crashes in native code might occur.</p>
     *
     * <p>The rules that should be followed by {@code srcBase}, {@code srcOffset}, {@code destBase}, {@code destOffset},
     * and {@code bytes} are the following:</p>
     *
     * <ul>
     *     <li>
     *         {@code srcBase} is a primitive array of type {@code C}, {@code srcOffset} is a value that was obtained
     *         through {@code B + I * S} (where {@code I} is a valid index into {@code srcBase}, {@code B} is the base
     *         offset of {@code srcBase} obtained by supplying {@code C} to {@link #arrayBaseOffset(Class)}, and
     *         {@code S} is the index scale of {@code srcBase} obtained by supplying {@code C} to
     *         {@link #arrayIndexScale(Class)}), {@code destBase} is a primitive array of type {@code T},
     *         {@code destOffset} is a value that was obtained through {@code D + J * L} (where {@code J} is a valid
     *         index into {@code destBase}, {@code D} is the base offset of {@code destBase} obtained by supplying
     *         {@code D} to {@link #arrayBaseOffset(Class)}, and {@code L} is the index scale of {@code destBase}
     *         obtained by supplying {@code D} to {@link #arrayIndexScale(Class)}), and {@code bytes} is an amount that
     *         identifies two memory regions, one that is bounded by {@code srcOffset} and {@code srcBase}'s length and
     *         another that is bounded by {@code destOffset} and {@code destBase}'s length;
     *     </li>
     *     <li>
     *         {@code srcBase} is a primitive array of type {@code C}, {@code srcOffset} is a value that was obtained
     *         through {@code B + I * S} (where {@code I} is a valid index into {@code srcBase}, {@code B} is the base
     *         offset of {@code srcBase} obtained by supplying {@code C} to {@link #arrayBaseOffset(Class)}, and
     *         {@code S} is the index scale of {@code srcBase} obtained by supplying {@code C} to
     *         {@link #arrayIndexScale(Class)}), {@code destBase} is {@code null}, {@code destOffset} is an absolute
     *         address that could be supplied to the second argument of {@link #memcpy(long, long, long)}, and
     *         {@code bytes} is an amount that identifies a memory region bounded by {@code srcOffset} and
     *         {@code srcBase}'s length;
     *     </li>
     *     <li>
     *         {@code srcBase} is {@code null}, {@code srcOffset} is an absolute address that could be supplied to the
     *         first argument of {@link #memcpy(long, long, long)}, {@code destBase} is a primitive array of type
     *         {@code T}, {@code destOffset} is a value that was obtained through {@code D + J * L} (where {@code J} is
     *         a valid index into {@code destBase}, {@code D} is the base offset of {@code destBase} obtained by
     *         supplying {@code D} to {@link #arrayBaseOffset(Class)}, and {@code L} is the index scale of
     *         {@code destBase} obtained by supplying {@code D} to {@link #arrayIndexScale(Class)}), and {@code bytes}
     *         is an amount that identifies a memory region bounded by {@code destOffset} and {@code destBase}'s length;
     *     </li>
     *     <li>
     *         {@code srcBase} is {@code null}, {@code srcOffset} is an absolute address that could be supplied to the
     *         first argument of {@link #memcpy(long, long, long)}, {@code destBase} is {@code null}, {@code destOffset}
     *         is an absolute address that could be supplied to the second argument of
     *         {@link #memcpy(long, long, long)}, {@code bytes} is any positive number.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode provided by the last of the four
     * cases is <strong>discouraged</strong>, as Java variables need not have the same memory layout as native variables
     * nor this fact can be considered portable across multiple JVM implementations.</p>
     *
     * <p>The memory stores are executed in atomic units whose size is determined by both the {@code bytes} parameter
     * and the actual addresses that are obtained from the <em>double-register</em>-like addressing mode, which will be
     * identified by {@code srcAddress} and {@code destAddress} in the following text. In practice, the JVM will verify
     * if {@code bytes} and {@code srcAddress} are the same number modulo {@code i} (where {@code i} assumes in order the
     * values {@code 8}, {@code 4}, {@code 2}, and {@code 1}). The first value of {@code i} that satisfies the equality
     * will determine the expected source size. The same operation is then repeated between {@code bytes} and
     * {@code destAddress} to determine the expected destination size. The minimum between the two expected sizes will
     * then determine the size of the atomic blocks. For example, if {@code srcAddress mod 8 == bytes mod 8} and
     * {@code destAddress mod 4 == bytes mod 4}, then bytes of size {@code 4} will be set atomically.</p>
     *
     * @param srcBase The {@link Object} that makes up the first part of the addressing mode for the source region.
     * @param srcOffset The offset that makes up the second part of the addressing mode for the source region.
     * @param destBase The {@link Object} that makes up the first part of the addressing mode for the destination
     *                 region.
     * @param destOffset The offset that makes up the second part of the addressing mode for the destination region.
     * @param bytes The length of the memory region that should be copied. It must be a non-negative number.
     * @throws RuntimeException <strong>Optionally</strong>, if the given arguments do not respect the conditions
     * detailed above or if {@code bytes} is a negative number. Note that this exception might not be thrown even if
     * these conditions are met due to aggressive JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static void memcpy(final Object srcBase, final long srcOffset, final Object destBase, final long destOffset, final long bytes) {
        UNSAFE.copyMemory(srcBase, srcOffset, destBase, destOffset, bytes);
    }

    /**
     * Copies all bytes from the given source memory region to the given destination.
     *
     * <p>The location of both memory regions is determined through a <em>single-register</em>-like addressing mode,
     * where the {@code long} value is used as an absolute address into JVM memory.</p>
     *
     * <p>Usage of this method to copy elements of primitive arrays instead of using the <em>double-register</em>-like
     * addressing mode provided by {@link #memcpy(Object, long, Object, long, long)} is <strong>discouraged</strong>, as
     * Java variables need not have the same memory layouts as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The memory stores are executed in atomic units whose size is determined by both the {@code bytes} parameter
     * and both {@code srcAddress} and {@code destAddress}. In practice, the JVM will verify if {@code bytes} and
     * {@code srcAddress} are the same number modulo {@code i} (where {@code i} assumes in order the values {@code 8},
     * {@code 4}, {@code 2}, and {@code 1}). The first value of {@code i} that satisfies the equality will determine the
     * expected source size. The same operation is then repeated between {@code bytes} and {@code destAddress} to
     * determine the expected destination size. The minimum between the two expected sizes will then determine the size
     * of the atomic blocks. For example, if {@code srcAddress mod 8 == bytes mod 8} and
     * {@code destAddress mod 4 == bytes mod 4}, then bytes of size {@code 4} will be set atomically.</p>
     *
     * @param srcAddress The address that marks the beginning of the source memory region for the copy.
     * @param destAddress The address that marks the beginning of the destination memory region for the copy.
     * @param bytes The length of the memory region that should be copied. It must be non-negative.
     * @throws RuntimeException <strong>Optionally</strong>, if the value of {@code bytes} is negative. Note that this
     * exception might not be thrown even if these conditions are met due to aggressive JVM optimizations that might be
     * performed.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static void memcpy(final long srcAddress, final long destAddress, final long bytes) {
        UNSAFE.copyMemory(srcAddress, destAddress, bytes);
    }

    /**
     * Frees a region of memory that was previously allocated.
     *
     * <p>It is the caller's responsibility to ensure that the pointer is referencing a memory region that has been
     * allocated through either {@link #malloc(long)} or {@link #realloc(long, long)}, or a null pointer. In the latter
     * case, the method does nothing</p>
     *
     * @param address A pointer into the memory region that should be freed, or a null pointer.
     *
     * @since 1.0.0
     */
    public static void free(final long address) {
        UNSAFE.freeMemory(address);
    }

    /**
     * Identifies the offset of a given non-static {@link Field} within the class that it is contained in.
     *
     * <p>It is guaranteed that two different {@code Field}s that reference the same underlying field will have the same
     * offset. At the same time, it is guaranteed that two different fields in the same class will have differing
     * offsets. On the contrary, though, the returned value is <strong>not</strong> a pointer, meaning that pointer
     * arithmetic on it is not defined and should not be performed. The returned value should thus be considered as
     * meaningless and only used for operations within {@link Unsafe}.</p>
     *
     * <p>No checks are performed on whether the field is non-static or not.</p>
     *
     * @param f The field of which the offset should be determined.
     * @return The offset of the field within its class.
     * @throws NullPointerException If the given field is {@code null}.
     * @throws UnsupportedOperationException If the field belongs to a {@linkplain Class#isHidden() hidden class} or to
     * a {@linkplain Class#isRecord() record}.
     *
     * @since 1.0.0
     */
    public static long objectFieldOffset(final Field f) {
        return UNSAFE.objectFieldOffset(f);
    }

    /**
     * Identifies the offset of a given static {@link Field}.
     *
     * <p>It is guaranteed that two different {@code Field}s that reference the same underlying field will have the same
     * offset. On the contrary, though, the returned value is <strong>not</strong> a pointer, meaning that pointer
     * arithmetic on it is not defined and should not be performed. The returned value should thus be considered as
     * meaningless and only used for operations within {@link Unsafe}.</p>
     *
     * <p>No checks are performed on whether the field is static or not.</p>
     *
     * <p>This method should always be used in conjunction with {@link #staticFieldBase(Field)} to fully determine the
     * location of a static field.</p>
     *
     * @param f The field of which the offset should be determined.
     * @return The offset of the static field.
     * @throws NullPointerException If the given field is {@code null}.
     * @throws UnsupportedOperationException If the field belongs to a {@linkplain Class#isHidden() hidden class} or to
     * a {@linkplain Class#isRecord() record}.
     *
     * @since 1.0.0
     */
    public static long staticFieldOffset(final Field f) {
        return UNSAFE.staticFieldOffset(f);
    }

    /**
     * Identifies the base object of a given static {@link Field}.
     *
     * <p>The returned {@link Object} might or might not be {@code null}, but its value should be considered meaningless
     * and only used for operations within {@link Unsafe}. It is in fact possible for the given {@code Object} to be a
     * fake shim and not correspond to a real object in JVM memory or somewhere else.</p>
     *
     * <p>No checks are performed on whether the field is static or not.</p>
     *
     * <p>This method should always be used in conjunction with {@link #staticFieldOffset(Field)} to fully determine the
     * location of a static field.</p>
     *
     * @param f The field of which the base {@link Object} should be determined.
     * @return The base {@link Object} of the static field, which might be {@code null}.
     * @throws NullPointerException If the given field is {@code null}.
     * @throws UnsupportedOperationException If the field belongs to a {@linkplain Class#isHidden() hidden class} or to
     * a {@linkplain Class#isRecord() record}.
     *
     * @since 1.0.0
     */
    public static Object staticFieldBase(final Field f) {
        return UNSAFE.staticFieldBase(f);
    }

    /**
     * Returns whether the given {@link Class} should be initialized.
     *
     * <p>If initialization is required, a client can then use {@link #ensureClassInitialized(Class)} with the same
     * argument to forcefully initialize the class if needed.</p>
     *
     * <p>The full definition of <em>initialize</em> is available in the Java Virtual Machine Specifications, §5.5.</p>
     *
     * @param c The {@link Class} that should be checked for initialization.
     * @return {@code false} if and only if calling {@link #ensureClassInitialized(Class)} would have no effect.
     * @throws NullPointerException If the given class is {@code null}.
     * @deprecated This method is deprecated for removal, with no API replacement. Since multiple threads might be
     * trying to initialize the same class and actions can be interleaved due to scheduling issues, the only meaningful
     * return value for this method is {@code false}. A value of {@code true}, in fact, holds only until a call to this
     * method is completed, as another thread might initialize the class before the caller has had a chance to invoke
     * {@link #ensureClassInitialized(Class)}. Users of this API should thus refer to
     * {@link java.lang.invoke.MethodHandles.Lookup#ensureInitialized(Class)} instead, which no-ops if the class has
     * already been initialized.
     *
     * @since 1.0.0
     */
    @Deprecated(forRemoval = true)
    @SuppressWarnings("removal")
    public static boolean shouldBeInitialized(final Class<?> c) {
        return UNSAFE.shouldBeInitialized(c);
    }

    /**
     * Ensures that the given {@link Class} is initialized.
     *
     * <p>In other words, if the given class {@linkplain #shouldBeInitialized(Class) should be initialized}, this method
     * will execute that operation. On the other hand, if initialization has already been performed. This method acts as
     * does nothing.</p>
     *
     * <p>The full definition of <em>initialize</em> is available in the Java Virtual Machine Specifications, §5.5.</p>
     *
     * @param c The {@link Class} that should be initialized
     * @throws NullPointerException If the given class is {@code null}.
     * @deprecated This method is deprecated for removal. Users of this API should refer to
     * {@link java.lang.invoke.MethodHandles.Lookup#ensureInitialized(Class)} instead, which implements this exact
     * behavior.
     *
     * @since 1.0.0
     */
    @Deprecated(forRemoval = true)
    @SuppressWarnings("removal")
    public static void ensureClassInitialized(Class<?> c) {
        UNSAFE.ensureClassInitialized(c);
    }

    /**
     * Identifies the base offset in the storage allocation of the given array {@link Class}.
     *
     * <p>It is guaranteed that the values for primitive and {@link Object} arrays will be the same as the ones
     * available through the static constants available in this class.</p>
     *
     * <p>The return value of this method can be used together with {@link #arrayIndexScale(Class)} to compute the
     * offset of any element into the array through the {@code B + I * S} formula, where {@code I} is a valid index into
     * the array, {@code S} is the index scale factor, and {@code B} is the base offset returned by this method.</p>
     *
     * <p>Note that the class type is not checked to ensure it actually represents an array type. Supplying a non-array
     * class type, then, could result in nonsensical values or cause crashes in native code.</p>
     *
     * @param arrayClass The array {@link Class} for which the base offset should be obtained.
     * @return The base offset of the array {@code Class}.
     * @throws NullPointerException If the given class is {@code null}.
     *
     * @since 1.0.0
     */
    public static int arrayBaseOffset(final Class<?> arrayClass) {
        return UNSAFE.arrayBaseOffset(arrayClass);
    }

    /**
     * Identifies the index scale in the storage allocation of the given array {@link Class}, if possible.
     *
     * <p>It is guaranteed that the values for primitive and {@link Object} arrays will be the same as the ones
     * available through the static constants available in this class.</p>
     *
     * <p>The return value of this method might be either {@code 0} or positive. In the first case, this indicates that
     * indexed array access is not available, which might be the case for types that are smaller than one byte in size.
     * If the result is positive, on the other hand, the value can be used together with {@link #arrayBaseOffset(Class)}
     * to compute the offset of any element into the array through the {@code B + I * S} formula, where {@code I} is a
     * valid index into the array, {@code S} is the index scale returned by this method, and {@code B} is the base
     * offset of the array.</p>
     *
     * <p>Note that the class type is not checked to ensure it actually represents an array type. Supplying a non-array
     * class type, then, could result in nonsensical values or cause crashes in native code.</p>
     *
     * @param arrayClass The array {@link Class} for which the index scale should be obtained.
     * @return The index scale of the array {@code Class} if available, {@code 0} otherwise.
     * @throws NullPointerException If the given class is {@code null}.
     *
     * @since 1.0.0
     */
    public static int arrayIndexScale(final Class<?> arrayClass) {
        return UNSAFE.arrayIndexScale(arrayClass);
    }

    /**
     * Gets the size used to represent a memory address.
     *
     * <p>In other words, the returned value corresponds to the size of a pointer on the target machine, also
     * represented by {@code size_t} in native code. It is guaranteed to be either {@code 4}, for 32-bit architectures,
     * or {@code 8}, for 64-bit architectures.</p>
     *
     * <p>The value returned by this method has no relation to the size of other primitive types.</p>
     *
     * <p>The value returned by this method is also available statically through {@link #ADDRESS_SIZE}.</p>
     *
     * @return The size used to represent a memory address.
     *
     * @since 1.0.0
     */
    public static int addressSize() {
        return UNSAFE.addressSize();
    }

    /**
     * Gets the size of a native memory page.
     *
     * <p>Differently from {@link #addressSize()}, no guarantees other than it being a power of two are made on the
     * value returned by this method.</p>
     *
     * <p>The value returned by this method is also available statically through {@link #PAGE_SIZE}.</p>
     *
     * @return The size of a native memory page.
     *
     * @since 1.0.0
     */
    public static int pageSize() {
        return UNSAFE.pageSize();
    }



    /**
     * Allocates an instance of the given {@link Class} without invoking the constructor.
     *
     * <p>In other words, this method allows for the allocation of a new object of the given {@code Class} that is left
     * in its initial state, meaning no constructors are run and no fields are initialized to values that are not their
     * default values. Setting of the various fields is left to the caller of the method, which can use methods such
     * as {@link #objectFieldOffset(Field)} and {@link #putInt(Object, long, int)} to set the values.</p>
     *
     * <p>The given {@code Class} is initialized if it has not yet been, as if {@link #ensureClassInitialized(Class)}
     * has been called.</p>
     *
     * <p>The actual type of the class is not considered by this method, which simply returns an {@link Object}. It is
     * the caller's responsibility to cast it to the correct type.</p>
     *
     * @param cls The {@link Class} of which an instance should be created.
     * @return A newly created instance of the given {@link Class}, in the form of an {@link Object}.
     * @throws InstantiationException If an error occurs during instance creation.
     *
     * @since 1.0.0
     */
    public static Object allocateInstance(final Class<?> cls) throws InstantiationException {
        return UNSAFE.allocateInstance(cls);
    }

    /**
     * Throws the given {@link Throwable}.
     *
     * <p>Differently from the normal {@code throw} statement, this method allows not only to bypass the verifier, but
     * also avoid having to explicitly catch or declare checked exceptions that are thrown. Note that this also prevents
     * callers of the method from catching these kinds of exceptions. Clients of this method should be aware of this
     * limitation.</p>
     *
     * @param ee The {@link Throwable} that should be thrown.
     *
     * @since 1.0.0
     */
    public static void throwException(final Throwable ee) {
        UNSAFE.throwException(ee);
    }



    /**
     * Executes a <em>compare-and-set</em> operation on the given reference-holding location.
     *
     * <p>A <em>compare-and-set</em> operation is an atomic operation that compares the given value against an
     * {@code expected} value. If the result of the comparison succeeds, then the value is updated to the new value
     * {@code x} as part of the same atomic operation. On the contrary, if comparison fails, no updates take place. Note
     * that the comparison is performed according to reference equality (i.e., as if with the {@code ==} operator), not
     * by leveraging {@link Object#equals(Object)}.</p>
     *
     * <p>The location of the reference that should undergo the given operation is determined by an
     * {@link Object}-{@code long} pair, that provide a <em>double-register</em>-like addressing mode. The two values
     * must be set according to the following rules:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an array of a certain {@link Class} {@code C}, and {@code offset} is a value that was
     *         obtained through {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code C} to
     *         {@link #arrayBaseOffset(Class)}, and {@code S} is the index scale of the array obtained by supplying
     *         {@code C} to {@link #arrayIndexScale(Class)}.
     *     </li>
     * </ul>
     *
     * <p>Not following one of the above conventions might result in either unexpected behavior or crashes in native
     * code. It is up to the client to ensure the given restrictions are followed.</p>
     *
     * <p>The variable will be both read and written according to {@code volatile} memory semantics.</p>
     *
     * <p>This operation is equivalent to the C11 {@code atomic_compare_exchange_strong} function.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The second part of the addressing mode.
     * @param expected The value that should be currently stored in the target location.
     * @param x The new value that should be set in the target location, if comparison succeeds.
     * @return Whether the comparison succeeded and the variable has been updated ({@code true}) or not ({@code false}).
     *
     * @since 1.0.0
     */
    public static boolean casReference(final Object o, final long offset, final Object expected, final Object x) {
        return UNSAFE.compareAndSwapObject(o, offset, expected, x);
    }

    /**
     * Executes a <em>compare-and-set</em> operation on the given {@code int}-holding location.
     *
     * <p>A <em>compare-and-set</em> operation is an atomic operation that compares the given value against an
     * {@code expected} value. If the result of the comparison succeeds, then the value is updated to the new value
     * {@code x} as part of the same atomic operation. On the contrary, if comparison fails, no updates take place.</p>
     *
     * <p>The location of the reference that should undergo the given operation is determined by an
     * {@link Object}-{@code long} pair, that provide a <em>double-register</em>-like addressing mode. The two values
     * must be set according to the following rules:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an {@code int} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code int[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_INT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code int[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_INT_INDEX_SCALE}.
     *     </li>
     * </ul>
     *
     * <p>Not following one of the above conventions might result in either unexpected behavior or crashes in native
     * code. It is up to the client to ensure the given restrictions are followed.</p>
     *
     * <p>The variable will be both read and written according to {@code volatile} memory semantics.</p>
     *
     * <p>This operation is equivalent to the C11 {@code atomic_compare_exchange_strong} function.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The second part of the addressing mode.
     * @param expected The value that should be currently stored in the target location.
     * @param x The new value that should be set in the target location, if comparison succeeds.
     * @return Whether the comparison succeeded and the variable has been updated ({@code true}) or not ({@code false}).
     *
     * @since 1.0.0
     */
    public static boolean casInt(final Object o, final long offset, final int expected, final int x) {
        return UNSAFE.compareAndSwapInt(o, offset, expected, x);
    }

    /**
     * Executes a <em>compare-and-set</em> operation on the given {@code long}-holding location.
     *
     * <p>A <em>compare-and-set</em> operation is an atomic operation that compares the given value against an
     * {@code expected} value. If the result of the comparison succeeds, then the value is updated to the new value
     * {@code x} as part of the same atomic operation. On the contrary, if comparison fails, no updates take place.</p>
     *
     * <p>The location of the reference that should undergo the given operation is determined by an
     * {@link Object}-{@code long} pair, that provide a <em>double-register</em>-like addressing mode. The two values
     * must be set according to the following rules:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code long} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code long[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_LONG_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code long[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_LONG_INDEX_SCALE}.
     *     </li>
     * </ul>
     *
     * <p>Not following one of the above conventions might result in either unexpected behavior or crashes in native
     * code. It is up to the client to ensure the given restrictions are followed.</p>
     *
     * <p>The variable will be both read and written according to {@code volatile} memory semantics.</p>
     *
     * <p>This operation is equivalent to the C11 {@code atomic_compare_exchange_strong} function.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The second part of the addressing mode.
     * @param expected The value that should be currently stored in the target location.
     * @param x The new value that should be set in the target location, if comparison succeeds.
     * @return Whether the comparison succeeded and the variable has been updated ({@code true}) or not ({@code false}).
     *
     * @since 1.0.0
     */
    public static boolean casLong(final Object o, final long offset, final long expected, final long x) {
        return UNSAFE.compareAndSwapLong(o, offset, expected, x);
    }



    /**
     * Obtains the {@code boolean} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code boolean}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code boolean} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code boolean[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_BOOLEAN_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code boolean[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_BOOLEAN_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getByte(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code boolean}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static boolean getBooleanVolatile(final Object o, final long offset) {
        return UNSAFE.getBooleanVolatile(o, offset);
    }

    /**
     * Obtains the {@code byte} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code byte}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code byte} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code byte[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_BYTE_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code byte[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_BYTE_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getByte(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code byte}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static byte getByteVolatile(final Object o, final long offset) {
        return UNSAFE.getByteVolatile(o, offset);
    }

    /**
     * Obtains the {@code char} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code char}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code char} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code char[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_CHAR_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code char[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_CHAR_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getChar(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code char}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static char getCharVolatile(final Object o, final long offset) {
        return UNSAFE.getCharVolatile(o, offset);
    }

    /**
     * Obtains the {@code double} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code double}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code double} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code double[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_DOUBLE_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code double[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_DOUBLE_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getDouble(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code double}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static double getDoubleVolatile(final Object o, final long offset) {
        return UNSAFE.getDoubleVolatile(o, offset);
    }

    /**
     * Obtains the {@code float} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code float}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code float} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code float[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_FLOAT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code float[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_FLOAT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getFloat(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code float}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static float getFloatVolatile(final Object o, final long offset) {
        return UNSAFE.getFloatVolatile(o, offset);
    }

    /**
     * Obtains the {@code int} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code int}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an {@code int} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code int[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_INT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code int[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_INT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getInt(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code int}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static int getIntVolatile(final Object o, final long offset) {
        return UNSAFE.getIntVolatile(o, offset);
    }

    /**
     * Obtains the {@code long} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code long}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code long} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code long[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_LONG_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code long[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_LONG_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getLong(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code long}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static long getLongVolatile(final Object o, final long offset) {
        return UNSAFE.getLongVolatile(o, offset);
    }

    /**
     * Obtains the reference value of the field or array element at the given location with <em>volatile</em> semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually a reference.
     * Note that no additional assumptions are made on the type of the object: it is up to client code to cast it back
     * to the correct type, if required. In any other case, the result is undefined. Take note that wrong arguments
     * might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an array of a certain {@link Class} {@code C}, and {@code offset} is a value that was
     *         obtained through {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code C} to
     *         {@link #arrayBaseOffset(Class)}, and {@code S} is the index scale of the array obtained by supplying
     *         {@code C} to {@link #arrayIndexScale(Class)}.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is unavailable, as native variables
     * cannot be properly represented via actual Java types.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is a reference type; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static Object getReferenceVolatile(final Object o, final long offset) {
        return UNSAFE.getObjectVolatile(o, offset);
    }

    /**
     * Obtains the {@code short} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code short}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code short} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code short[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_SHORT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code short[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_SHORT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getShort(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @return The value of the pointed field or array element, if its type is {@code short}; otherwise the results
     * are undefined.
     *
     * @since 1.0.0
     */
    public static short getShortVolatile(final Object o, final long offset) {
        return UNSAFE.getShortVolatile(o, offset);
    }

    /**
     * Sets the {@code boolean} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code boolean}. Therefore, the value can be properly set if and only
     * if both {@code o} and {@code offset} have been obtained according to the rules outlined in the following
     * paragraph and the field or array element pointed by them is actually of type {@code boolean}. In any other case,
     * the behavior is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code boolean} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code boolean[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_BOOLEAN_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code boolean[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_BOOLEAN_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putByte(long, byte)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putBooleanVolatile(final Object o, final long offset, final boolean x) {
        UNSAFE.putBooleanVolatile(o, offset, x);
    }

    /**
     * Sets the {@code byte} value of the field or array element at the given location with <em>volatile</em> semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code byte}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code byte}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code byte} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code byte[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_BYTE_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code byte[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_BYTE_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putByte(long, byte)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putByteVolatile(final Object o, final long offset, final byte x) {
        UNSAFE.putByteVolatile(o, offset, x);
    }

    /**
     * Sets the {@code char} value of the field or array element at the given location with <em>volatile</em> semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code char}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code char}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code char} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code char[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_CHAR_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code char[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_CHAR_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putChar(long, char)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putCharVolatile(final Object o, final long offset, final char x) {
        UNSAFE.putCharVolatile(o, offset, x);
    }

    /**
     * Sets the {@code double} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code double}. Therefore, the value can be properly set if and only
     * if both {@code o} and {@code offset} have been obtained according to the rules outlined in the following
     * paragraph and the field or array element pointed by them is actually of type {@code double}. In any other case,
     * the behavior is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code double} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code double[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_DOUBLE_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code double[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_DOUBLE_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putDouble(long, double)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putDoubleVolatile(final Object o, final long offset, final double x) {
        UNSAFE.putDoubleVolatile(o, offset, x);
    }

    /**
     * Sets the {@code float} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code float}. Therefore, the value can be properly set if and only
     * if both {@code o} and {@code offset} have been obtained according to the rules outlined in the following
     * paragraph and the field or array element pointed by them is actually of type {@code float}. In any other case,
     * the behavior is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code float} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code float[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_FLOAT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code float[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_FLOAT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putFloat(long, float)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putFloatVolatile(final Object o, final long offset, final float x) {
        UNSAFE.putFloatVolatile(o, offset, x);
    }

    /**
     * Sets the {@code int} value of the field or array element at the given location with <em>volatile</em> semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code int}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code int}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an {@code int} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code int[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_INT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code int[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_INT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putInt(long, int)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putIntVolatile(final Object o, final long offset, final int x) {
        UNSAFE.putIntVolatile(o, offset, x);
    }

    /**
     * Sets the {@code long} value of the field or array element at the given location with <em>volatile</em> semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code long}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code long}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code long} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code long[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_LONG_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code long[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_LONG_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putLong(long, long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putLongVolatile(final Object o, final long offset, final long x) {
        UNSAFE.putLongVolatile(o, offset, x);
    }

    /**
     * Sets the reference value of the field or array element at the given location with <em>volatile</em> semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually contains a reference, nor that it has the same type as the runtime type of
     * {@code x}. Therefore, the value can be properly set if and only if both {@code o} and {@code offset} have been
     * obtained according to the rules outlined in the following paragraph and the field or array element pointed by
     * them can actually hold a reference with the same type of {@code x}, meaning that {@code x.getClass()} is the same
     * or a subclass of the field or array element. In any other case, the behavior is undefined. Take note that wrong
     * arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an array of a certain {@link Class} {@code C}, and {@code offset} is a value that was
     *         obtained through {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code C} to
     *         {@link #arrayBaseOffset(Class)}, and {@code S} is the index scale of the array obtained by supplying
     *         {@code C} to {@link #arrayIndexScale(Class)}.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is unavailable, as native variables
     * cannot be properly represented via actual Java types.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putReferenceVolatile(final Object o, final long offset, final Object x) {
        UNSAFE.putObjectVolatile(o, offset, x);
    }

    /**
     * Sets the {@code short} value of the field or array element at the given location with <em>volatile</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code short}. Therefore, the value can be properly set if and only
     * if both {@code o} and {@code offset} have been obtained according to the rules outlined in the following
     * paragraph and the field or array element pointed by them is actually of type {@code short}. In any other case,
     * the behavior is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code short} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code short[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_SHORT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code short[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_SHORT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putShort(long, short)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putShortVolatile(final Object o, final long offset, final short x) {
        UNSAFE.putShortVolatile(o, offset, x);
    }



    /**
     * Sets the {@code int} value of the field or array element at the given location with <em>release/acquire</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code int}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code int}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an {@code int} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code int[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_INT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code int[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_INT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putInt(long, int)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>release/acquire</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putIntRelease(final Object o, final long offset, final int x) {
        UNSAFE.putOrderedInt(o, offset, x);
    }

    /**
     * Sets the {@code long} value of the field or array element at the given location with <em>release/acquire</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually of type {@code long}. Therefore, the value can be properly set if and only if
     * both {@code o} and {@code offset} have been obtained according to the rules outlined in the following paragraph
     * and the field or array element pointed by them is actually of type {@code long}. In any other case, the behavior
     * is undefined. Take note that wrong arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code long} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code long[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_LONG_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code long[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_LONG_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #putLong(long, long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The value will be written according to <em>release/acquire</em> semantics.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putLongRelease(final Object o, final long offset, final long x) {
        UNSAFE.putOrderedLong(o, offset, x);
    }

    /**
     * Sets the reference value of the field or array element at the given location with <em>release/acquire</em>
     * semantics.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>There are no validation checks performed on the actual type of the field or array element pointed by the given
     * location to ensure that it actually contains a reference, nor that it has the same type as the runtime type of
     * {@code x}. Therefore, the value can be properly set if and only if both {@code o} and {@code offset} have been
     * obtained according to the rules outlined in the following paragraph and the field or array element pointed by
     * them can actually hold a reference with the same type of {@code x}, meaning that {@code x.getClass()} is the same
     * or a subclass of the field or array element. In any other case, the behavior is undefined. Take note that wrong
     * arguments might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an array of a certain {@link Class} {@code C}, and {@code offset} is a value that was
     *         obtained through {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code C} to
     *         {@link #arrayBaseOffset(Class)}, and {@code S} is the index scale of the array obtained by supplying
     *         {@code C} to {@link #arrayIndexScale(Class)}.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is unavailable, as native variables
     * cannot be properly represented via actual Java types.</p>
     *
     * <p>The value will be written according to <em>release/acquire</em> semantics</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param x The value that should be put into the location.
     *
     * @since 1.0.0
     */
    public static void putReferenceRelease(final Object o, final long offset, final Object x) {
        UNSAFE.putOrderedObject(o, offset, x);
    }



    /**
     * Unparks the given {@link Thread}, if {@linkplain #park(boolean, long) parked}.
     *
     * <p>If the {@code Thread} was not parked previously, then the subsequent call to {@link #park(boolean, long)} will
     * be ignored. Essentially, the invocation of this method will be "remembered" until the next time the thread has
     * been parked.</p>
     *
     * <p>Note that the method accepts any {@link Object} as an argument. It is the caller's responsibility to ensure
     * that the given object is a reference to an existing thread (i.e., in Java, a {@link Thread} instance). Not
     * respecting this condition might cause crashes in native code.</p>
     *
     * @param thread The {@link Thread} that should not unparked.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static void unpark(final Object thread) {
        UNSAFE.unpark(thread);
    }

    /**
     * Parks the current {@link Thread}, blocking its execution until this method returns.
     *
     * <p>In other words, a {@code Thread} should be considered parked only if it currently "busy" executing this
     * method. A thread will thus exit the parked state when this method returns. In turn, this means that the thread
     * will not be blocked anymore and will be free to continue execution of its code.</p>
     *
     * <p>If the given {@code Thread} had an {@linkplain #unpark(Object) un-park request} "pending", then this method
     * will return immediately. Otherwise, the thread will park until either a corresponding un-park request occurs or,
     * if {@code time} has a non-zero value, either the specified amount of {@code time} passes (if {@code absolute} is
     * {@code false}) or the given Epoch time is hit (if {@code absolute} is {@code true}).</p>
     *
     * <p>Take note that a thread might also exit its parked state if it gets
     * {@linkplain Thread#interrupt() interrupted} or for any other spurious reasons as determined by the scheduler.</p>
     *
     * @param isAbsolute Indicates whether {@code time} should be treated as an Epoch instant ({@code true}) or as an
     *                   amount of milliseconds ({@code false}). This value is ignored if {@code time} is {@code 0}.
     * @param time The maximum amount of time that the {@link Thread} should remain in its parked state, or the Epoch
     *             at which the {@code Thread} should resume execution, or {@code 0}. The last value indicates that no
     *             limit on the amount of time has been placed.
     *
     * @since 1.0.0
     */
    public static void park(final boolean isAbsolute, final long time) {
        UNSAFE.park(isAbsolute, time);
    }



    /**
     * Obtains the load average in the system run queue assigned to the available processors averaged over various
     * periods of time.
     *
     * <p>The amount of samples retrieved is given by {@code nSamples} and the values are placed in the
     * {@code loadAverage} array from index {@code 0} to {@code nSamples - 1}.</p>
     *
     * <p>It is not allowed to request more than {@code 3} samples, corresponding to averages over the last 1, 5, and 15
     * minutes respectively. Note that the system might always return less samples than the requested ones: callers of
     * this method should always verify the return value.</p>
     *
     * @param loadAverage The array in which the load average samples should be stored in.
     * @param nSamples The amount of samples to retrieve: it must be between {@code 1} and {@code 3} inclusive.
     * @return The amount of samples actually retrieved, or {@code -1} if the load average cannot be obtained.
     * @throws NullPointerException If {@code loadAverage} is {@code null}.
     * @throws ArrayIndexOutOfBoundsException If {@code nSamples} is outside the range of validity, or is bigger than
     * the length of {@code loadAverage}.
     *
     * @since 1.0.0
     */
    public static int getLoadAverage(final double[] loadAverage, final int nSamples) {
        return UNSAFE.getLoadAverage(loadAverage, nSamples);
    }



    /**
     * Atomically obtains the {@code int} value of the field or array element at the given location and adds the given
     * {@code delta} to it.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code int}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an {@code int} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code int[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_INT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code int[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_INT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getInt(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>Both the read operation and the addition will be performed in a single atomic operation.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param delta The amount that should be added to the value.
     * @return The value of the pointed field or array element before the update operation occurred, if its type is
     * {@code int}; otherwise the results are undefined.
     *
     * @since 1.0.0
     */
    public static int getAndAddInt(final Object o, final long offset, final int delta) {
        return UNSAFE.getAndAddInt(o, offset, delta);
    }

    /**
     * Atomically obtains the {@code long} value of the field or array element at the given location and adds the given
     * {@code delta} to it.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code long}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code long} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code long[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_LONG_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code long[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_LONG_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getLong(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>Both the read operation and the addition will be performed in a single atomic operation.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param delta The amount that should be added to the value.
     * @return The value of the pointed field or array element before the update operation occurred, if its type is
     * {@code long}; otherwise the results are undefined.
     *
     * @since 1.0.0
     */
    public static long getAndAddLong(final Object o, final long offset, final long delta) {
        return UNSAFE.getAndAddLong(o, offset, delta);
    }

    /**
     * Atomically exchanges the {@code int} value of the field or array element at the given location with a new value.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code int}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an {@code int} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code int[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_INT_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code int[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_INT_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getInt(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The exchange operation is performed atomically, with the old value being returned by this method.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param newValue The new value that should be set.
     * @return The value of the pointed field or array element before the exchange operation occurred, if its type is
     * {@code int}; otherwise the results are undefined.
     *
     * @since 1.0.0
     */
    public static int getAndSetInt(final Object o, final long offset, final int newValue) {
        return UNSAFE.getAndSetInt(o, offset, newValue);
    }

    /**
     * Atomically exchanges the {@code long} value of the field or array element at the given location with a new value.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually of type
     * {@code int}. In any other case, the result is undefined. Take note that wrong arguments might lead to crashes
     * in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is a {@code long} array, and {@code offset} is a value that was obtained through
     *         {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code long[].class} to
     *         {@link #arrayBaseOffset(Class)} or by using {@link #ARRAY_LONG_BASE_OFFSET}, and {@code S} is the
     *         index scale of the array obtained by supplying {@code long[].class} to {@link #arrayIndexScale(Class)}
     *         or by using {@link #ARRAY_LONG_INDEX_SCALE};
     *     </li>
     *     <li>
     *         {@code o} is {@code null} and {@code offset} is a valid value that can be supplied to
     *         {@link #getLong(long)}, in which case this method will act as if it were invoked in a
     *         <em>single-register</em>-like addressing mode.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is <strong>discouraged</strong>, as Java
     * variables need not have the same memory layout as native variables nor this fact can be considered portable
     * across multiple JVM implementations.</p>
     *
     * <p>The exchange operation is performed atomically, with the old value being returned by this method.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param newValue The new value that should be set.
     * @return The value of the pointed field or array element before the exchange operation occurred, if its type is
     * {@code long}; otherwise the results are undefined.
     *
     * @since 1.0.0
     */
    public static long getAndSetLong(final Object o, final long offset, final long newValue) {
        return UNSAFE.getAndSetLong(o, offset, newValue);
    }

    /**
     * Atomically exchanges the reference value of the field or array element at the given location with a new value.
     *
     * <p>The location of the field or array element is determined through a <em>double-register</em>-like addressing
     * mode, where the {@link Object} {@code o} and the {@code offset} are used to determine the actual address in the
     * JVM memory.</p>
     *
     * <p>The value returned by this method is defined if and only if the arguments have been obtained according to the
     * rules outlined in the following paragraph and the field or array element pointed by them is actually a reference.
     * Note that no additional assumptions are made on the type of the object: it is up to client code to cast it back
     * to the correct type, if required. In any other case, the result is undefined. Take note that wrong arguments
     * might lead to crashes in native code.</p>
     *
     * <p>The values of both {@code o} and {@code offset} must be obtained in one of the following ways:</p>
     *
     * <ul>
     *     <li>
     *         {@code offset} was obtained by calling {@link #objectFieldOffset(Field)} with the non-{@code static}
     *         {@link Field} of a certain Java {@link Class} {@code C}, and {@code o} is an object whose class is
     *         {@code C} or a compatible subclass;
     *     </li>
     *     <li>
     *         {@code o} was obtained by calling {@link #staticFieldBase(Field)} with the {@code static} {@link Field}
     *         whose value must be obtained, and {@code offset} was obtained by calling
     *         {@link #staticFieldOffset(Field)} with the same {@code Field} instance;
     *     </li>
     *     <li>
     *         {@code o} is an array of a certain {@link Class} {@code C}, and {@code offset} is a value that was
     *         obtained through {@code B + I * S}, where {@code I} is the valid index of the element into the array,
     *         {@code B} is the base offset of the array obtained by supplying {@code C} to
     *         {@link #arrayBaseOffset(Class)}, and {@code S} is the index scale of the array obtained by supplying
     *         {@code C} to {@link #arrayIndexScale(Class)}.
     *     </li>
     * </ul>
     *
     * <p>Note that usage of the <em>single-register</em>-like addressing mode is unavailable, as native variables
     * cannot be properly represented via actual Java types.</p>
     *
     * <p>The exchange operation is performed atomically, with the old value being returned by this method.</p>
     *
     * @param o The {@link Object} that makes up the first part of the addressing mode.
     * @param offset The offset that makes up the second part of the addressing mode.
     * @param newValue The new value that should be set.
     * @return The value of the pointed field or array element before the exchange operation occurred, if its type is
     * a reference type; otherwise the results are undefined.
     *
     * @since 1.0.0
     */
    public static Object getAndSetReference(final Object o, final long offset, final Object newValue) {
        return UNSAFE.getAndSetObject(o, offset, newValue);
    }



    /**
     * Marks a loading fence according to the <em>release/acquire</em> semantics.
     *
     * <p>In particular, no load operations that occur before the fence will be reordered with loads and stores that
     * occur after the fence, applying what is also known as an <em>acquire</em> fence.</p>
     *
     * <p>This method corresponds to the C11 {@code atomic_thread_fence(memory_order_acquire)} operation.</p>
     *
     * @since 1.0.0
     */
    public static void loadFence() {
        UNSAFE.loadFence();
    }

    /**
     * Marks a storing fence according to the <em>release/acquire</em> semantics.
     *
     * <p>In particular, no load or store operations that occur before the fence will be reordered with stores that
     * occur after the fence, applying what is also known as a <em>release</em> fence.</p>
     *
     * <p>This method corresponds to the C11 {@code atomic_thread_fence(memory_order_release)} operation.</p>
     *
     * @since 1.0.0
     */
    public static void storeFence() {
        UNSAFE.storeFence();
    }

    /**
     * Marks a loading and storing fence (also known as full fence) according to the <em>release/acquire</em> semantics.
     *
     * <p>This method acts as both {@link #loadFence()} and {@link #storeFence()} together, essentially ensuring that
     * no load or store operations that occur before the fence will be reordered with loads and stores that occur after
     * the fence.</p>
     *
     * <p>This method corresponds to the C11 {@code atomic_thread_fence(memory_order_seq_cst)} operation.</p>
     *
     * @since 1.0.0
     */
    public static void fullFence() {
        UNSAFE.fullFence();
    }



    /**
     * Invokes the given {@link ByteBuffer}'s {@link java.lang.ref.Cleaner Cleaner} instance, if it exists.
     *
     * <p>The given buffer must be a {@linkplain ByteBuffer#isDirect() direct} {@code ByteBuffer}, and cannot be neither
     * a {@linkplain ByteBuffer#slice() slice} nor a {@linkplain ByteBuffer#duplicate() duplicate}.</p>
     *
     * @param directBuffer The {@link ByteBuffer} whose {@link java.lang.ref.Cleaner Cleaner} should be invoked.
     * @throws NullPointerException If {@code directBuffer} is {@code null}.
     * @throws IllegalArgumentException If {@code directBuffer} is not a {@linkplain ByteBuffer#isDirect() direct}
     * buffer, or if it is either a {@linkplain ByteBuffer#slice() slice} or a
     * {@linkplain ByteBuffer#duplicate() duplicate}.
     *
     * @since 1.0.0
     */
    public static void invokeCleaner(final ByteBuffer directBuffer) {
        UNSAFE.invokeCleaner(directBuffer);
    }
}
