/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/**
 * Provides a set of utilities that allow easier access to {@link sun.misc.Unsafe} features.
 *
 * <p>{@code Unsafe} allows Java code to perform low-level operations such as manual memory allocation or pointer
 * dereference. Due to the nature of these operations, normally only trusted code is allowed to access this API.
 * Moreover, attempting to use this class triggers a series of warnings in the compiler that might pollute the build
 * log.</p>
 *
 * <p>The utilities in this package allow clients to refer to {@code Unsafe} without worrying about these restrictions
 * through the usage of the {@link com.dwarveddonuts.neverwinter.unsafe.Unsafe} class. Moreover, an alternative for more
 * strongly-typed access is also provided in the form of the {@link com.dwarveddonuts.neverwinter.unsafe.TypedUnsafe}
 * class, along with additional helpers.</p>
 *
 * <p>No additional components are required for this component to work properly. It is also the client's responsibility
 * to ensure that the {@code jdk.unsupported} module is available on the module path.</p>
 *
 * @since 1.0.0
 */
package com.dwarveddonuts.neverwinter.unsafe;
