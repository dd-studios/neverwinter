/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.unsafe;

import java.util.Objects;

/**
 * Represents the address at which a Java field or array location is stored.
 *
 * <p>This class is essentially a wrapper around the {@link Object}-{@code long} addressing mode used by the JVM to
 * reference Java fields.</p>
 *
 * <p>A field address can be either in an <em>invalid</em> or in a <em>valid</em> state. The rules for determining this
 * will be described in the paragraph that follows. A field address obtained through {@link TypedUnsafe} might be in
 * either of those states; if the field needs to be passed to a method in {@code TypedUnsafe}, then it must be in a
 * <em>valid</em> state.</p>
 *
 * <p>A field address is defined in a valid state if at least one of the following is true:</p>
 *
 * <ul>
 *     <li>
 *         The field address points to a {@code static} field whose type is not an array;
 *     </li>
 *     <li>
 *         The field address points to a non-{@code static} field whose type is not an array, and the field address has
 *         been {@linkplain #bindTo(Object) bound} to an {@link Object};
 *     </li>
 *     <li>
 *         The field address points to a field whose type is an array, and the field address has been both
 *         {@linkplain #bindTo(Object) bound} to an array of the correct type and {@linkplain #atIndex(int) indexed}
 *         by a certain address.
 *     </li>
 * </ul>
 *
 * <p>Instances of this class cannot be constructed directly, but can be obtained through {@code TypedUnsafe} by usage
 * of the {@link TypedUnsafe#fieldAddress(java.lang.reflect.Field)} method. Refer to the indications provided in that
 * method to know how to operate with an instance of this class before using it.</p>
 *
 * @param <T> The type of the field pointed to by this address.
 *
 * @since 1.0.0
 */
public abstract sealed class FieldAddress<T> {
    private static final class StaticFieldAddress<T> extends FieldAddress<T> {
        StaticFieldAddress(final Object owner, final long offset, final Class<T> type) {
            super(owner, offset, type);
        }

        @Override
        public FieldAddress<T> bindTo(final Object o) {
            throw new UnsupportedOperationException("Unable to bind a static field address to an object");
        }

        @Override
        public FieldAddress<T> atIndex(final int index) {
            throw new UnsupportedOperationException("Unable to change the index of a static field address");
        }

        @Override
        void validate() {}
    }

    private static final class ObjectFieldAddress<T> extends FieldAddress<T> {
        ObjectFieldAddress(final Object owner, final long offset, final Class<T> type) {
            super(owner, offset, type);
        }

        @Override
        public FieldAddress<T> bindTo(final Object owner) {
            return new ObjectFieldAddress<>(owner, this.offset(), this.type());
        }

        @Override
        public FieldAddress<T> atIndex(final int index) {
            throw new UnsupportedOperationException("Unable to change the index of an object field address");
        }

        @Override
        void validate() {
            if (this.owner() == null) {
                throw new IllegalStateException("Object field address has not been bound");
            }
        }
    }

    private static final class ArrayFieldAddress<T> extends FieldAddress<T> {
        private static final long INVALID_OFFSET = Long.MIN_VALUE;

        private final Long offset;
        private final int baseOffset;
        private final int scale;

        ArrayFieldAddress(final Object owner, final Long offset, final int baseOffset, final int scale, final Class<T> type) {
            super(owner, INVALID_OFFSET, type);
            this.offset = offset;
            this.baseOffset = baseOffset;
            this.scale = scale;
        }

        @Override
        public FieldAddress<T> bindTo(final Object o) {
            return new ArrayFieldAddress<>(this.owner(), this.offset, this.baseOffset, this.scale, this.type());
        }

        @Override
        public FieldAddress<T> atIndex(final int index) {
            final long offset = this.baseOffset + (((long) index) * this.scale);
            return new ArrayFieldAddress<>(this.owner(), offset, this.baseOffset, this.scale, this.type());
        }

        @Override
        public long offset() {
            return Objects.requireNonNullElse(this.offset, INVALID_OFFSET);
        }

        @Override
        void validate() {
            if (this.owner() == null) {
                throw new IllegalStateException("Array field address has not been bound");
            }
            if (!this.owner().getClass().isArray()) {
                throw new IllegalStateException("Array field address is bound to non-array " + this.owner());
            }
            if (this.offset == null) {
                throw new IllegalStateException("Array field index has not been specified");
            }
        }
    }

    private final Object owner;
    private final long offset;
    private final Class<T> type;

    protected FieldAddress(final Object owner, final long offset, final Class<T> type) {
        this.owner = owner;
        this.offset = offset;
        this.type = Objects.requireNonNull(type);
    }

    static <T> FieldAddress<T> ofStatic(final Object owner, final long offset, final Class<T> type) {
        return new StaticFieldAddress<>(owner, offset, type);
    }

    static <T> FieldAddress<T> ofObject(final long offset, final Class<T> type) {
        return new ObjectFieldAddress<>(null, offset, type);
    }

    static <T> FieldAddress<T> ofArray(final int baseOffset, final int scale, final Class<T> type) {
        return new ArrayFieldAddress<>(null, null, baseOffset, scale, type);
    }

    /**
     * Binds this address to the given {@link Object}.
     *
     * <p>Binding an address to an {@code Object} is used to identify the object whose data should be accessed in case
     * of a non-{@code static} field or of an array. It is possible to bind a field address multiple times, in which
     * case only the last binding will be validated.</p>
     *
     * <p>Binding might be required to obtain a valid field address as determined by the rules outlined in the
     * {@linkplain FieldAddress main documentation}.</p>
     *
     * @param o The {@link Object} the field address should be bound to.
     * @return A copy of this address, bound to the given {@link Object}.
     * @throws UnsupportedOperationException If this field address cannot be bound to an {@code Object}, such as for a
     * {@code static} field.
     *
     * @since 1.0.0
     */
    public abstract FieldAddress<T> bindTo(final Object o);

    /**
     * Indexes this address at the given value.
     *
     * <p>Indexing an address is used to identify the position into the array that should be accessed, if inside the
     * bounds of the given array. No bounds checking is performed by this method. It is possible to index a field
     * address multiple times, in which case only the last indexing will be validated.</p>
     *
     * <p>Indexing might be required to obtain a valid field address as determined by the rules outlined in the
     * {@linkplain FieldAddress main documentation}.</p>
     *
     * @param index The index that the array pointed by this field should be indexed at.
     * @return A copy of this address, indexed at the given value.
     * @throws UnsupportedOperationException If this field address cannot be indexed, such as for any field whose type
     * is not an array.
     *
     * @since 1.0.0
     */
    public abstract FieldAddress<T> atIndex(final int index);

    /**
     * Gets the owner of the given field address.
     *
     * <p>Note that the returned {@link Object} might be {@code null} in case the address has not been
     * {@linkplain #bindTo(Object) bound} yet. Moreover, the returned value might also be a fake shim (which can be
     * either {@code null} or not) used to represent the owner of a {@code static} field. In other words, clients should
     * not assume that the object returned by this field is a regular object.</p>
     *
     * @return The owner.
     *
     * @since 1.0.0
     */
    public Object owner() {
        return this.owner;
    }

    /**
     * Gets the offset of the given field address.
     *
     * <p>Note that the offset is a meaningless value and should not be treated as a pointer, as this kind of
     * relationship between Java variables and native memory might not exist and would definitely not be portable across
     * different JVM implementations. Clients should therefore consider this value as a simple sentinel value used
     * only by the JVM internally.</p>
     *
     * @return The offset.
     *
     * @since 1.0.0
     */
    public long offset() {
        return this.offset;
    }

    /**
     * Gets the {@link Class} representing the type stored by this address.
     *
     * @return The type.
     *
     * @since 1.0.0
     */
    public Class<T> type() {
        return this.type;
    }

    <U> T cast(final U o) {
        return this.type.cast(o);
    }

    abstract void validate();

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final FieldAddress<?> that = (FieldAddress<?>) o;
        return this.offset() == that.offset() && Objects.equals(this.owner(), that.owner());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.owner(), this.offset());
    }

    @Override
    public String toString() {
        return '[' + Objects.toString(this.owner()) + '@' + Long.toUnsignedString(this.offset(), 16) + ']';
    }
}
