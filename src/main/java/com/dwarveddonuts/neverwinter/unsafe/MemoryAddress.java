/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.unsafe;

import java.util.Objects;

/**
 * Represents a native memory address as returned by {@link TypedUnsafe}.
 *
 * <p>This class is essentially a wrapper around a pointer into native memory, allowing usage of native addresses in a
 * more type-safe way.</p>
 *
 * <p>Every memory address references a memory region which might be either <em>valid</em> or <em>invalid</em>. A memory
 * region is defined as <em>invalid</em> if and only if the {@linkplain #address() pointer} stored by this class is a
 * null pointer (i.e., {@code 0}). In any other case, the memory address references a <em>valid</em> region.</p>
 *
 * <p>Moreover, a memory address can be either <em>untyped</em> or <em>typed</em>. A memory address is considered
 * <em>untyped</em>if its {@linkplain #type() type} is set to {@code null}. The address is otherwise <em>typed</em>.
 * Take note that the type in this context is not assumed to be a strong type, and an untyped pointer can become typed
 * and vice versa, through the {@link #changeType(Class)} method. This is because native memory addresses store data
 * which is by nature untyped and it is up to the developer to interpret the raw numbers.</p>
 *
 * <p>Instances of this class cannot be constructed directly, but rather can only be obtained through the methods in
 * {@link TypedUnsafe} returning them, such as {@link TypedUnsafe#malloc(long)} and
 * {@link TypedUnsafe#realloc(MemoryAddress, long)}.</p>
 *
 * <p>This class allows manipulation of the internal pointer structure through the {@link #offset(long)} method. It is
 * up to the caller to ensure that the new memory address remains valid, as no bound checking is performed due to
 * performance concerns.</p>
 *
 * @param <T> The type of the values stored at the given memory address.
 *
 * @since 1.0.0
 */
public final class MemoryAddress<T> {
    private final long address;
    private final Class<T> type;

    private MemoryAddress(final long address, final Class<T> type) {
        this.address = address;
        this.type = type;
    }

    static <T> MemoryAddress<T> of(final long address, final Class<T> type) {
        return new MemoryAddress<>(address, Objects.requireNonNull(type));
    }

    static MemoryAddress<?> ofUntyped(final long address) {
        return new MemoryAddress<>(address, null);
    }

    /**
     * Reinterprets the data stored at this memory address as if it were from the given {@link Class}.
     *
     * <p>Note that no checking is performed to ensure that this reinterpretation makes sense: it is up to clients of
     * this library to verify this. Moreover, this does not alter the memory contents: it is simply a "rename" of the
     * type.</p>
     *
     * <p>This method can also be used to obtain a typed pointer from an untyped one or vice versa. The latter is
     * discouraged, though, as a matter of type-safety.</p>
     *
     * @param newType The new type that the {@link MemoryAddress} should have.
     * @return A new {@link MemoryAddress} with the same address, but with the new type.
     * @param <U> The type that the pointer should have.
     *
     * @since 1.0.0
     */
    public <U> MemoryAddress<U> changeType(final Class<U> newType) {
        return new MemoryAddress<>(this.address(), newType);
    }

    /**
     * Offsets this pointer by the given amount.
     *
     * <p>The amount is always assumed to be in bytes, regardless of the generic type of the memory address. This is
     * in contrast with C pointer semantics, but it is done for both performance reasons and to make the API easier to
     * use.</p>
     *
     * <p>With this method, it is possible to move both forwards and backwards in memory by specifying either a positive
     * or negative offset. Care must be taken not to go outside of the bounds of the allocated memory region. No bound
     * checking is performed by this function.</p>
     *
     * @param offset The offset that should be added to the current address.
     * @return A new {@link MemoryAddress} pointing to the offset specified and with the same type as the original, if
     * present.
     *
     * @since 1.0.0
     */
    public MemoryAddress<T> offset(final long offset) {
        return new MemoryAddress<>(this.address() + offset, this.type());
    }

    /**
     * Gets the address pointed to by this memory address.
     *
     * <p>If the value is {@code 0}, then this pointer is to be considered as pointing to an <em>invalid</em> region of
     * memory. On the contrary, if the value is different from {@code 0}, the address points to a <em>valid</em> region
     * of memory.</p>
     *
     * @return The address.
     *
     * @since 1.0.0
     */
    public long address() {
        return this.address;
    }

    /**
     * Gets the {@link Class} representing the type stored by this address, if any.
     *
     * <p>If the address is <em>untyped</em>, then this method will return {@code null}. Otherwise, the address is
     * considered <em>typed</em>.</p>
     *
     * @return The type, if any.
     *
     * @since 1.0.0
     */
    public Class<T> type() {
        return this.type;
    }

    <U> T cast(final U u) {
        if (this.type == null) {
            throw new IllegalStateException("Invariant broken: cast attempted with untyped address");
        }
        return this.type.cast(u);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        final MemoryAddress<?> that = (MemoryAddress<?>) o;
        return this.address() == that.address();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.address());
    }

    @Override
    public String toString() {
        return Long.toUnsignedString(this.address(), 16);
    }
}
