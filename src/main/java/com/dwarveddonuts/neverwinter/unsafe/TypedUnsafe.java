/*
 * MIT License
 *
 * Copyright (c) 2022 Dwarved Donuts Studios
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.dwarveddonuts.neverwinter.unsafe;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * Provides a more type-safe and object-oriented API to access {@link Unsafe}.
 *
 * <p>The methods provided in this class allow access to the same set of methods, excluding the ones that have been
 * {@linkplain Deprecated deprecated for removal}. The difference resides in an API that has been altered to allow for
 * more type-safety and error-checking that allows for easier debugging, trying to avoid as many crashes in native JVM
 * code as possible.</p>
 *
 * <p>At the same time, clients that want maximum performance, might want to stick to the raw API, as the additional
 * checks and generic-based method signatures might cause some performance loss with repeated invocations. Profiling is
 * nevertheless suggested.</p>
 *
 * <p>The same set of warnings that apply to {@code Unsafe} also apply to the methods available in this class.</p>
 *
 * @since 1.0.0
 */
public final class TypedUnsafe {
    /**
     * Holds the size used to represent a memory address.
     *
     * <p>In other words, this number corresponds to the size of a pointer on the target machine, also represented by
     * {@code size_t} in native code. It is guaranteed to be either {@code 4}, for 32-bit architectures, or {@code 8},
     * for 64-bit architectures.</p>
     *
     * @since 1.0.0
     */
    public static final int ADDRESS_SIZE = Unsafe.ADDRESS_SIZE;
    /**
     * Holds the size of a native memory page.
     *
     * <p>Differently from {@link #ADDRESS_SIZE}, no guarantees other than it being a power of two are made on this
     * value.</p>
     *
     * @since 1.0.0
     */
    public static final int PAGE_SIZE = Unsafe.PAGE_SIZE;

    private TypedUnsafe() {}

    /**
     * Obtains the value of the field or array element pointed to by the given {@code address}.
     *
     * <p>The given {@link FieldAddress} must be valid, i.e. pointing to a valid field or array location. Note that the
     * checks performed are only superficial to avoid incurring in too big of a performance penalty: the client should
     * thus take care to avoid giving invalid data to this method to prevent crashes in native code.</p>
     *
     * @param address The {@link FieldAddress} of the field whose value should be obtained.
     * @return The value of the pointed field or array element.
     * @param <T> The type of the value contained in the pointed field or array element.
     * @throws NullPointerException If the {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the {@link FieldAddress} does not point to a valid location.
     * @throws IllegalArgumentException If the type of the field cannot be properly determined or obtained. Note that
     * this exception should never be thrown in normal circumstances.
     *
     * @since 1.0.0
     */
    public static <T> T get(final FieldAddress<T> address) {
        address.validate(); // Implicit null-check

        final Object o = address.owner();
        final long offset = address.offset();
        final Class<T> type = address.type();

        if (type == boolean.class) {
            return address.cast(Unsafe.getBoolean(o, offset));
        }
        if (type == byte.class) {
            return address.cast(Unsafe.getByte(o, offset));
        }
        if (type == char.class) {
            return address.cast(Unsafe.getChar(o, offset));
        }
        if (type == double.class) {
            return address.cast(Unsafe.getDouble(o, offset));
        }
        if (type == float.class) {
            return address.cast(Unsafe.getFloat(o, offset));
        }
        if (type == int.class) {
            return address.cast(Unsafe.getInt(o, offset));
        }
        if (type == long.class) {
            return address.cast(Unsafe.getLong(o, offset));
        }
        if (type == short.class) {
            return address.cast(Unsafe.getShort(o, offset));
        }
        if (Object.class.isAssignableFrom(type)) {
            return address.cast(Unsafe.getReference(o, offset));
        }

        throw new IllegalArgumentException("Unknown or unsupported type " + type.getName() + " for field address " + address);
    }

    /**
     * Sets the value of the field or array element pointed to by the given address.
     *
     * <p>The given {@link FieldAddress} must be valid, i.e. point to a valid field or array location. Note that the
     * performed checks are only superficial to incur in too big of a performance penalty; the client should thus take
     * care to avoid giving invalid data to this method to prevent crashes in native code.</p>
     *
     * @param address The {@link FieldAddress} whose value should be set.
     * @param value The value to set the given field or array element to.
     * @param <T> The type contained in the field or array element.
     * @throws NullPointerException If the {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the {@link FieldAddress} does not point to a valid location.
     * @throws IllegalArgumentException If the type of the field cannot be properly determined or obtained. Note that
     * this exception should never be thrown in normal circumnstances.
     *
     * @since 1.0.0
     */
    public static <T> void put(final FieldAddress<T> address, final T value) {
        address.validate(); // Implicit null-check

        final Object o = address.owner();
        final long offset = address.offset();
        final Class<T> type = address.type();

        if (type == boolean.class) {
            Unsafe.putBoolean(o, offset, (boolean) value);
        } else if (type == byte.class) {
            Unsafe.putByte(o, offset, (byte) value);
        } else if (type == char.class) {
            Unsafe.putChar(o, offset, (char) value);
        } else if (type == double.class) {
            Unsafe.putDouble(o, offset, (double) value);
        } else if (type == float.class) {
            Unsafe.putFloat(o, offset, (float) value);
        } else if (type == int.class) {
            Unsafe.putInt(o, offset, (int) value);
        } else if (type == long.class) {
            Unsafe.putLong(o, offset, (long) value);
        } else if (type == short.class) {
            Unsafe.putShort(o, offset, (short) value);
        } else if (Object.class.isAssignableFrom(type)) {
            Unsafe.putReference(o, offset, value);
        } else {
            throw new IllegalArgumentException("Unknown or unsupported type " + type.getName() + " for field address " + address);
        }
    }

    /**
     * Obtains the value of the given type stored at the specified memory address.
     *
     * <p>The given {@link MemoryAddress} must be {@linkplain MemoryAddress#type() typed} according to the value stored.
     * Moreover, the address should point into a memory region allocated through {@link #malloc(long)}, otherwise the
     * results are undefined. No validation is performed on the values to avoid incurring in too big of a performance
     * penalty: it is still the client responsibility to ensure that valid data is provided to this method to avoid
     * crashes in native code.</p>
     *
     * <p>Note that clients <strong>cannot</strong> obtain a <em>pointer</em>'s value with this method, due to
     * ambiguities. Refer to {@link #getAddress(MemoryAddress)} if that behavior is required.</p>
     *
     * @param address The {@link MemoryAddress} whose value should be obtained.
     * @return The value stored at the given {@link MemoryAddress}, if possible.
     * @param <T> The type of the value stored at the given address.
     * @throws NullPointerException If the {@link MemoryAddress} is {@code null} or untyped.
     * @throws IllegalStateException If the type of the {@link MemoryAddress} cannot be obtained (e.g., because it is a
     * reference type or a {@code boolean}). Note that no checks on the validity of the obtained type are performed.
     *
     * @since 1.0.0
     */
    public static <T> T get(final MemoryAddress<T> address) {
        final long offset = address.address(); // Implicit null-check
        final Class<T> type = address.type(); // Implicit null-check of type in the error message

        if (type == byte.class) {
            return address.cast(Unsafe.getByte(offset));
        }
        if (type == char.class) {
            return address.cast(Unsafe.getChar(offset));
        }
        if (type == double.class) {
            return address.cast(Unsafe.getDouble(offset));
        }
        if (type == float.class) {
            return address.cast(Unsafe.getFloat(offset));
        }
        if (type == int.class) {
            return address.cast(Unsafe.getInt(offset));
        }
        if (type == long.class) {
            return address.cast(Unsafe.getLong(offset));
        }
        if (type == short.class) {
            return address.cast(Unsafe.getShort(offset));
        }

        throw new IllegalStateException("Unknown or unsupported type " + type.getName() + " for address " + address);
    }

    /**
     * Obtains the pointer stored at the specified memory address.
     *
     * <p>The given {@link MemoryAddress} must point into a memory region allocated through {@link #malloc(long)},
     * otherwise the results are undefined. No validation to ensure this fact is performed to avoid incurring in too big
     * of a performance penalty: it is still the client responsibility to ensure that valid data is provided to this
     * method to avoid crashes in native code.</p>
     *
     * <p>The obtained value is guaranteed to be a {@code long}. If pointers on a specific JVM implementations are less
     * than 64-bits wide, then the result is treated as an unsigned {@code long} and extended to be 64-bits in size.
     * Clients that want to verify the actual size of the pointer can refer to {@link #ADDRESS_SIZE} for the size in
     * bytes.</p>
     *
     * <p>The pointer behaves exactly as any other pointer in native code, meaning that pointer arithmetic is defined.
     * A client that wishes to offset the pointer by a certain amount of bytes can simply do so by adding that number to
     * the result of this method.</p>
     *
     * @param address The {@link MemoryAddress} where the pointer is stored.
     * @return The pointer stored at the given address.
     * @throws NullPointerException If the {@link MemoryAddress} is {@code null}.
     *
     * @since 1.0.0
     */
    public static long getAddress(final MemoryAddress<?> address) {
        return Unsafe.getAddress(address.address()); // Implicit null-check
    }

    /**
     * Stores the given value to the given memory location.
     *
     * <p>The given {@link MemoryAddress} must be {@linkplain MemoryAddress#type() typed} according to the value that
     * the client wishes to store. Moreover, the address should point into a memory region allocated through
     * {@link #malloc(long)}, otherwise the behavior is undefined. No validation is performed on the values to avoid
     * incurring in too big of a performance penalty: it is still the client responsibility to ensure that valid data is
     * provided to this method to avoid crashes in native code.</p>
     *
     * <p>Note that clients <strong>cannot</strong> store a <em>pointer</em> with this method, due to ambiguities. Refer
     * to {@link #putAddress(MemoryAddress, long)} if that behavior is desired.</p>
     *
     * @param address The {@link MemoryAddress} where the value should be stored.
     * @param value The value to store at the given address.
     * @param <T> The type of the value that should be stored at the given address.
     * @throws NullPointerException If the {@link MemoryAddress} is {@code null} or untyped.
     * @throws IllegalStateException If the type of the {@link MemoryAddress} cannot be obtained (e.g., because it is a
     * reference type or a {@code boolean}). Note that no checks on the validity of the obtained type are performed.
     *
     * @since 1.0.0
     */
    public static <T> void put(final MemoryAddress<T> address, final T value) {
        final long offset = address.address(); // Implicit null-check
        final Class<T> type = address.type(); // Implicit null-check  of type in the error message

        if (type == byte.class) {
            Unsafe.putByte(offset, (byte) value);
        } else if (type == char.class) {
            Unsafe.putChar(offset, (char) value);
        } else if (type == double.class) {
            Unsafe.putDouble(offset, (double) value);
        } else if (type == float.class) {
            Unsafe.putFloat(offset, (float) value);
        } else if (type == int.class) {
            Unsafe.putInt(offset, (int) value);
        } else if (type == long.class) {
            Unsafe.putLong(offset, (long) value);
        } else if (type == short.class) {
            Unsafe.putShort(offset, (short) value);
        } else {
            throw new IllegalStateException("Unknown or unsupported type " + type.getName() + " for address " + address);
        }
    }

    /**
     * Stores the given pointer at the specified memory address.
     *
     * <p>The given {@link MemoryAddress} must point into a memory region allocated through {@link #malloc(long)},
     * otherwise the behavior is undefined. No validation to ensure this fact is performed to avoid incurring in too big
     * of a performance penalty: it is still the client responsibility to ensure that valid data is provided to this
     * method to avoid crashes in native code.</p>
     *
     * <p>If pointers on a specific JVM implementation are less than 64-bits wide, then the result is truncated to the
     * correct size before being written, with the upper-most exceeding bits being discarded. Clients that want to
     * verify the actual size of the pointer written can refer to {@link #ADDRESS_SIZE} for the size in bytes.</p>
     *
     * @param address The {@link MemoryAddress} where the pointer should be stored.
     * @param value The value to write at the given address.
     * @throws NullPointerException If the {@link MemoryAddress} is {@code null}.
     *
     * @since 1.0.0
     */
    public static void putAddress(final MemoryAddress<?> address, final long value) {
        Unsafe.putAddress(address.address(), value); // Implicit null-check
    }



    /**
     * Allocates a native memory region of the given size.
     *
     * <p>The memory region that will be allocated will be aligned for all value types, meaning that the size specified
     * might be rounded up (<em>never</em> down) to account for proper alignment.</p>
     *
     * <p>The contents of the allocated memory region are not cleared out and thus any attempts to read into areas that
     * have not been written before will likely result in garbage data. Clients that would like to initialize memory to
     * a specific value should invoke the {@link #memset(MemoryAddress, long, byte)} method after allocation.</p>
     *
     * <p>It is the caller's responsibility to free the memory region once it has no more use for it through the
     * {@link #free(MemoryAddress)} method. No garbage collection mechanisms will be allowed to modify or reclaim the
     * given area of memory.</p>
     *
     * <p>The memory region allocated has a static size, namely the one specified by {@code bytes}. Callers that wish to
     * alter the allocated memory area may do so with {@link #realloc(MemoryAddress, long)}.</p>
     *
     * @param bytes The amount of bytes that need to be allocated. It must be a positive number.
     * @return An untyped {@link MemoryAddress} referencing the first byte of the allocated memory region; the address
     * is guaranteed to always reference a valid memory region.
     * @throws OutOfMemoryError If the allocation was refused because of size constraints.
     * @throws RuntimeException <strong>Optionally</strong>, if the given memory size is invalid either because it is
     * negative or is of a type unrepresentable by the native platform (e.g., trying to allocate more than 3 GBs on a
     * 32-bit platform). Note that this exception might not be thrown even if these conditions are met due to aggressive
     * JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    public static MemoryAddress<?> malloc(final long bytes) {
        return MemoryAddress.ofUntyped(Unsafe.malloc(bytes));
    }

    /**
     * Resizes an already existing memory region to the given size.
     *
     * <p>The resized memory region will still be aligned for all value types, meaning that the size specified might be
     * rounded up (<em>never</em> down) to account for proper alignment.</p>
     *
     * <p>The behavior of this method changes depending on the values given to both {@code address} and {@code bytes}
     * as determined by the following rules:</p>
     *
     * <ul>
     *     <li>
     *         If {@code address} references a valid memory region and {@code bytes} is not zero, then the specified
     *         memory region is resized to the given size if possible. If the size is increased, then the "added" space
     *         is not cleared out and will likely contain garbage data; it is possible for a caller to use
     *         {@link #memset(MemoryAddress, long, byte)} to manually clear the region if needed. On the other hand, if
     *         the size is decreased, the "excluded" space is automatically reclaimed. It is then the caller's
     *         responsibility to manually free the memory region through {@link #free(MemoryAddress)} once it has
     *         exhausted its purpose, or to alter the size of the region again with this same method.
     *     </li>
     *     <li>
     *         If {@code address} references an invalid memory region and {@code bytes} is not zero, then this method
     *         behaves exactly as {@link #malloc(long)} as if it were called with {@code bytes} as its only argument.
     *     </li>
     *     <li>
     *         If {@code address} references a valid memory region and {@code bytes} is zero, then this method behaves
     *         exactly as {@link #free(MemoryAddress)} as if it were called with {@code address} as its only argument.
     *     </li>
     *     <li>
     *         If {@code address} references an invalid memory region and {@code bytes} is zero, then this method does
     *         nothing.
     *     </li>
     * </ul>
     *
     * @param address The {@link MemoryAddress} referencing either an invalid region, or the region that has to be
     *                resized.
     * @param bytes The new size of the memory region. It must be a positive number or zero to free the given region.
     * @return A {@link MemoryAddress} with the same type as the original pointing to the resized region, or to an
     * invalid region if the given one was freed, according to the rules outlined before. Note that clients may not
     * assume that {@code address} and the return value coincide, as no such guarantees are provided by the runtime:
     * once this method has been called, clients should only refer to the memory region through the new address provided
     * by this method.
     * @throws NullPointerException If the {@link MemoryAddress} is {@code null} or untyped.
     * @throws OutOfMemoryError If the reallocation was refused because of size constraints.
     * @throws RuntimeException <strong>Optionally</strong>, if the given memory size is invalid either because it is
     * negative or is of a type unrepresentable by the native platform (e.g. trying to reallocate a memory region to be
     * of more than 3 GBs in size on a 32-bit platform). Note that this exception might not be thrown even if these
     * conditions are met due to aggressive JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    public static <T> MemoryAddress<T> realloc(final MemoryAddress<T> address, final long bytes) {
        final Class<T> type = Objects.requireNonNull(address.type()); // Both implicit and explicit null-check
        return MemoryAddress.of(Unsafe.realloc(address.address(), bytes), type);
    }

    /**
     * Sets the bytes of the given field-determined region to the given value.
     *
     * <p>The field that should be set is determined through the given {@link FieldAddress}es. The given field must not
     * only be valid, but it should also reference an array location within a primitive array. At the same time,
     * {@code bytes} should be within the region bounded by the given {@code FieldAddress} and the array's length. If
     * these contraints are not followed, then crashes in native code may end up occurring.</p>
     *
     * <p>The validation performed on the paramters is very superficial and does not verify the actual correctness of
     * the fields and {@code bytes} according to the above constraints due to performance reasons: it is still the
     * caller's responsibility to ensure that valid values are passed to this method.</p>
     *
     * <p>The memory stores are executed in atomic units whose size is determined by both the bytes parameter and the
     * actual address at which the {@link FieldAddress} is stored, which will be identified by <em>address</em> in the
     * following text. In practice, the JVM will verify if {@code bytes} and <em>address</em> are the same number modulo
     * {@code i} (where {@code i} assumes in order the values {@code 8}, {@code 4}, {@code 2}, and {@code 1}). The first
     * value of {@code i} that satisfies the equality will determine the size of the atomic blocks. For example, if
     * {@code address mod 8 == bytes mod 8}, then bytes of size {@code 8} will be set atomically.</p>
     *
     * <p>If the user of this method wishes to set a memory region instead, they should refer to
     * {@link #memset(MemoryAddress, long, byte)}.</p>
     *
     * @param address The {@link FieldAddress} identifying the array location where the setting should begin.
     * @param bytes The amount of bytes that should be set. It cannot be negative.
     * @param value The value the various bytes should be set to.
     * @param <T> The type of the array location pointed to by the {@link FieldAddress}.
     * @throws NullPointerException If the {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the {@link FieldAddress} is invalid.
     * @throws RuntimeException <strong>Optionally</strong>, if the given arguments do not respect the conditions
     * detailed above or if {@code bytes} is a negative number. Note that this exception might not be thrown even if
     * these conditions are met due to aggressive JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static <T> void memset(final FieldAddress<T> address, final long bytes, final byte value) {
        address.validate(); // Implicit null-check
        // TODO("Stronger validation given the above much stricter constraints?")

        Unsafe.memset(address.owner(), address.offset(), bytes, value);
    }

    /**
     * Sets the bytes of the given memory region to the given value.
     *
     * <p>The memory region pointed to by the {@link MemoryAddress} must be within a valid memory region allocated
     * through {@link #malloc(long)}, and {@code bytes} should indicate an area that is fully contained within the given
     * {@link MemoryAddress} and the boundaries of the corresponding region. If these contraints are not followed, then
     * crashes in native code may end up occurring.</p>
     *
     * <p>The validation performed on the paramters is very superficial and does not verify the actual correctness of
     * the fields and {@code bytes} according to the above constraints due to performance reasons: it is still the
     * caller's responsibility to ensure that valid values are passed to this method.</p>
     *
     * <p>The memory stores are executed in atomic units whose size is determined by both the bytes parameter and the
     * address specified by the {@link MemoryAddress}, which will be identified by <em>address</em> in the following
     * text. In practice, the JVM will verify if {@code bytes} and <em>address</em> are the same number modulo {@code i}
     * (where {@code i} assumes in order the values {@code 8}, {@code 4}, {@code 2}, and {@code 1}). The first value of
     * {@code i} that satisfies the equality will determine the size of the atomic blocks. For example, if
     * {@code address mod 8 == bytes mod 8}, then bytes of size {@code 8} will be set atomically.</p>
     *
     * <p>If the user of this method wishes to set an array stored in a Java field instead, they should refer to
     * {@link #memset(FieldAddress, long, byte)}.</p>
     *
     * @param address The {@link MemoryAddress} identifying the beginning of the memory region to set.
     * @param bytes The amount of bytes that should be set. It cannot be negative.
     * @param value The value the various bytes should be set to.
     * @throws NullPointerException If the {@link MemoryAddress} is {@code null}.
     * @throws RuntimeException <strong>Optionally</strong>, if the given arguments do not respect the conditions
     * detailed above or if {@code bytes} is a negative number. Note that this exception might not be thrown even if
     * these conditions are met due to aggressive JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static void memset(final MemoryAddress<?> address, final long bytes, final byte value) {
        Unsafe.memset(address.address(), bytes, value); // Implicit null-check
    }

    /**
     * Copies all bytes from the given field-determined memory region to the given destination.
     *
     * <p>Both source and destination are to be determined through the given {@link FieldAddress}es. Both of them
     * must not only be valid, but they should also reference an array location within a primitive array. At the same
     * time, {@code bytes} should be within the regions bounded by the two pairs of {@code FieldAddress} and array's
     * length. If these contraints are not followed, then crashes in native code may end up occurring. It is also worth
     * noting that the two values need not refer to arrays of the same type, in which case the copy might result in the
     * values chaning meaning when read from Java (i.e. a {@code long} being read as two {@code int}s instead).</p>
     *
     * <p>The memory stores are executed in atomic units whose size is determined by both the {@code bytes} parameter
     * and the actual addresses that are obtained from the given {@link FieldAddress}es, which will be identified by
     * {@code srcAddress} and {@code destAddress} in the following text. In practice, the JVM will verify if
     * {@code bytes} and {@code srcAddress} are the same number modulo {@code i} (where {@code i} assumes in order the
     * values {@code 8}, {@code 4}, {@code 2}, and {@code 1}). The first value of {@code i} that satisfies the equality
     * will determine the expected source size. The same operation is then repeated between {@code bytes} and
     * {@code destAddress} to determine the expected destination size. The minimum between the two expected sizes will
     * then determine the size of the atomic blocks. For example, if {@code srcAddress mod 8 == bytes mod 8} and
     * {@code destAddress mod 4 == bytes mod 4}, then bytes of size {@code 4} will be set atomically.</p>
     *
     * <p>If the user of this method wishes to copy memory between two other memory regions instead, they should refer
     * to the {@link #memcpy(MemoryAddress, MemoryAddress, long)} method.</p>
     *
     * @param src The {@link FieldAddress} identifying the array location source of the copy.
     * @param dest The {@link FieldAddress} identifying the array location destination of the copy.
     * @param bytes The amount of bytes that should be copied between the two arrays. It cannot be negative.
     * @param <T> The type stored at the source array location. It need not match the destination's.
     * @param <U> The type stored at the destination array location. It need not match the source's.
     * @throws NullPointerException If either the {@code src} or the {@code dest} {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If either the {@code src} or the {@code dest} {@link FieldAddress} is invalid.
     * @throws RuntimeException <strong>Optionally</strong>, if the given arguments do not respect the conditions
     * detailed above or if {@code bytes} is a negative number. Note that this exception might not be thrown even if
     * these conditions are met due to aggressive JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static <T, U> void memcpy(final FieldAddress<T> src, final FieldAddress<U> dest, final long bytes) {
        src.validate(); // Implicit null-check
        dest.validate(); // Implicit null-check
        // TODO("Stronger validation given the above much stricter constraints?")

        Unsafe.memcpy(src.owner(), src.offset(), dest.owner(), dest.offset(), bytes);
    }

    /**
     * Copies all bytes from the given memory region to the given destination.
     *
     * <p>Both source and destination fields are to be determined through the given {@link MemoryAddress}es. Both of
     * them must point into valid memory regions that have been allocated with {@link #malloc(long)}. At the same time,
     * {@code bytes} should be within the regions bounded by each pair of {@code MemoryAddress} and corresponding region
     * boundaries. If these contraints are not followed, then crashes in native code may end up occurring. It is also
     * worth noting that the two {@code MemoryRegion}s are treated as untyped, meaning that the values need not match in
     * type between them. After the copy, then, the values may thus be interpreted differently.</p>
     *
     * <p>The memory stores are executed in atomic units whose size is determined by both the {@code bytes} parameter
     * and the actual addresses that are obtained from the given {@link MemoryAddress}es, which will be identified by
     * {@code srcAddress} and {@code destAddress} in the following text. In practice, the JVM will verify if
     * {@code bytes} and {@code srcAddress} are the same number modulo {@code i} (where {@code i} assumes in order the
     * values {@code 8}, {@code 4}, {@code 2}, and {@code 1}). The first value of {@code i} that satisfies the equality
     * will determine the expected source size. The same operation is then repeated between {@code bytes} and
     * {@code destAddress} to determine the expected destination size. The minimum between the two expected sizes will
     * then determine the size of the atomic blocks. For example, if {@code srcAddress mod 8 == bytes mod 8} and
     * {@code destAddress mod 4 == bytes mod 4}, then bytes of size {@code 4} will be set atomically.</p>
     *
     * <p>If the user of this method wishes to copy memory between two field-backed arrays, they should refer to the
     * {@link #memcpy(FieldAddress, FieldAddress, long)} method.</p>
     *
     * @param src The {@link MemoryAddress} identifying the source of the copy.
     * @param dest The {@link MemoryAddress} identifying the destination of the copy.
     * @param bytes The amount of bytes that should be copied between the two regions. It cannot be negative.
     * @throws NullPointerException If either the {@code src} or the {@code dest} {@link MemoryAddress} is {@code null}.
     * @throws RuntimeException <strong>Optionally</strong>, if the given arguments do not respect the conditions
     * detailed above or if {@code bytes} is a negative number. Note that this exception might not be thrown even if
     * these conditions are met due to aggressive JVM optimizations that might be performed.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static void memcpy(final MemoryAddress<?> src, final MemoryAddress<?> dest, final long bytes) {
        Unsafe.memcpy(src.address(), dest.address(), bytes); // Implicit null-check
    }

    // TODO("memcpy(Field, Memory) and memcpy(Memory, Field)")

    /**
     * Frees a region of memory that was previously allocated.
     *
     * <p>It is the caller's responsibility to ensure that the given {@link MemoryAddress} references a region of
     * memory that had been previously allocated through {@link #malloc(long)} or {@link #realloc(MemoryAddress, long)}.
     * Alternatively, the given memory region must be an invalid region, in which case nothing occurs.</p>
     *
     * @param address The {@link MemoryAddress} referencing the region that should be freed.
     * @throws NullPointerException If the {@link MemoryAddress} is {@code null}.
     *
     * @since 1.0.0
     */
    public static void free(final MemoryAddress<?> address) {
        Unsafe.free(address.address()); // Implicit null-check
    }



    /**
     * Obtains the {@link FieldAddress} for the given Java {@link Field}.
     *
     * <p>The given {@code Field} is examined reflectively to determine the kind and return an appropriate
     * {@code FieldAddress} that can represent it. The returned address may then require additional preprocessing before
     * it can be used, as determined by the following:</p>
     *
     * <ul>
     *     <li>
     *         If the given {@code Field} is {@code static} and not an array, then the returned value does not require
     *         further processing;
     *     </li>
     *     <li>
     *         If the given {@code Field} is non-{@code static} and not an array, then the returned value requires being
     *         {@linkplain FieldAddress#bindTo(Object) bound} to an {@link Object} prior to its usage;
     *     </li>
     *     <li>
     *         If the given {@code Field} is an array, then the returned value needs to be
     *         {@linkplain FieldAddress#bindTo(Object) bound} to an array and
     *         {@linkplain FieldAddress#atIndex(int) indexed} prior to its usage.
     *     </li>
     * </ul>
     *
     * <p>It is the caller's responsibility to ensure that the type of the field address corresponds to the generic type
     * returned by this method.</p>
     *
     * @param f The {@link Field} whose address should be determined.
     * @return A {@link FieldAddress} referencing the given field.
     * @param <T> The type of the given {@code Field}.
     * @throws NullPointerException If the given {@link Field} is {@code null}.
     * @throws UnsupportedOperationException If the given field is not an array type and belongs to a
     * {@linkplain Class#isHidden() hidden class} or a {@linkplain Class#isRecord() record}.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("unchecked")
    public static <T> FieldAddress<T> fieldAddress(final Field f) {
        final Class<T> type = (Class<T>) f.getType();
        final boolean isStatic = Modifier.isStatic(f.getModifiers());
        final boolean isArray = type.isArray();

        if (isArray) {
            return FieldAddress.ofArray(Unsafe.arrayBaseOffset(type), Unsafe.arrayIndexScale(type), type);
        }
        if (isStatic) {
            return FieldAddress.ofStatic(Unsafe.staticFieldBase(f), Unsafe.staticFieldOffset(f), type);
        }

        return FieldAddress.ofObject(Unsafe.objectFieldOffset(f), type);
    }



    /**
     * Allocates an instance of the given {@link Class} without invoking the constructor.
     *
     * <p>In other words, this method allows for the allocation of a new object of the given {@code Class} that is left
     * in its initial state, meaning no constructors are run and no fields are initialized to values that are not their
     * default values. Setting of the various fields is left to the caller of the method, which can use methods such
     * as {@link #fieldAddress(Field)} and {@link #put(FieldAddress, Object)} to set the values.</p>
     *
     * <p>The given {@code Class} is initialized if it has not yet been.</p>
     *
     * @param clazz The {@link Class} of which an instance should be created.
     * @return A newly created instance of the given {@link Class}.
     * @throws InstantiationException If an error occurs during instance creation.
     *
     * @since 1.0.0
     */
    public static <T> T allocateDirectly(final Class<T> clazz) throws InstantiationException {
        return clazz.cast(Unsafe.allocateInstance(clazz));
    }

    /**
     * Throws the given {@link Throwable}.
     *
     * <p>Differently from the normal {@code throw} statement, this method allows not only to bypass the verifier, but
     * also avoid having to explicitly catch or declare checked exceptions that are thrown. Note that this also prevents
     * callers of the method from catching these kinds of exceptions. Clients of this method should be aware of this
     * limitation.</p>
     *
     * <p>Note that while the method is marked as returning a {@link RuntimeException}, this is done purely to allow for
     * chaining by the clients, so that they do not have to throw a dummy exception themselves or perform other code
     * flow manipulations. Essentially, the usual pattern for using this method is
     * {@code throw TypedUnsafe.sneakyThrow(myThrowable)}. The usage of a {@link RuntimeException} allows for the usage
     * of the pattern even in the absence of checked exceptions.</p>
     *
     * @param throwable The {@link Throwable} that should be thrown.
     * @return A {@link RuntimeException}, for chaining.
     *
     * @since 1.0.0
     */
    public static RuntimeException sneakyThrow(final Throwable throwable) {
        Unsafe.throwException(throwable);
        return new RuntimeException(); // Unreachable code: throwException immediately returns
    }




    /**
     * Executes a <em>compare-and-set</em> operation on the given {@link FieldAddress}.
     *
     * <p>A <em>compare-and-set</em> operation is an atomic operation that compares the given value against an
     * {@code expected} value. If the result of the comparison succeeds, then the value is updated to the new value
     * {@code value} as part of the same atomic operation. On the contrary, if comparison fails, no updates take
     * place. In all cases, comparison is performed through pure equality, i.e. as if the {@code ==} operator is
     * used.</p>
     *
     * <p>The given {@link FieldAddress} must be valid, i.e. point to a valid field or array location. Moreover, only
     * certain types are supported for <em>compare-and-set</em> operations: {@code int}s, {@code long}s, and references.
     * Any other type will be ignored. Note that the performed checks are only superficial to incur in too big of a
     * performance penalty; the client should thus take care to avoid giving invalid data to this method to prevent
     * crashes in native code.</p>
     *
     * <p>The variable will be both read and written according to {@code volatile} memory semantics.</p>
     *
     * <p>This operation is equivalent to the C11 {@code atomic_compare_exchange_strong} function.</p>
     *
     * @param address The {@link FieldAddress} referencing the field for the <em>compare-and-set</em> operation.
     * @param expected The value that should be currently stored in the given location.
     * @param value The value that will be set in the target location, if the comparison succeeds.
     * @return Whether the comparison succeeded ({@code true}) or not ({@code false}), and consequently whether the
     * value was updated.
     * @param <T> The type of the value stored in the given {@link FieldAddress}.
     * @throws NullPointerException If the given {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the given {@link FieldAddress} is invalid.
     * @throws IllegalArgumentException If the type of the given {@link FieldAddress} cannot undergo a
     * <em>compare-and-set</em> operation.
     *
     * @since 1.0.0
     */
    public static <T> boolean cas(final FieldAddress<T> address, final T expected, final T value) {
        // TODO("Verify if validation is required")
        address.validate(); // Implicit null-check

        final Object o = address.owner();
        final long offset = address.offset();
        final Class<T> type = address.type();

        if (type == int.class) {
            return Unsafe.casInt(o, offset, (int) expected, (int) value);
        }
        if (type == long.class) {
            return Unsafe.casLong(o, offset, (int) expected, (int) value);
        }
        if (Object.class.isAssignableFrom(type)) {
            return Unsafe.casReference(o, offset, expected, value);
        }

        throw new IllegalArgumentException("Unknown or unsupported type " + type.getName() + " for address " + address);
    }



    /**
     * Obtains the value of the field or array element pointed to by the given {@code address} with <em>volatile</em>
     * semantics.
     *
     * <p>The given {@link FieldAddress} must be valid, i.e. pointing to a valid field or array location. Note that the
     * checks performed are only superficial to avoid incurring in too big of a performance penalty: the client should
     * thus take care to avoid giving invalid data to this method to prevent crashes in native code.</p>
     *
     * <p>The value will be read according to <em>volatile</em> semantics.</p>
     *
     * @param address The {@link FieldAddress} of the field whose value should be obtained.
     * @return The value of the pointed field or array element.
     * @param <T> The type of the value contained in the pointed field or array element.
     * @throws NullPointerException If the {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the {@link FieldAddress} does not point to a valid location.
     * @throws IllegalArgumentException If the type of the field cannot be properly determined or obtained. Note that
     * this exception should never be thrown in normal circumstances.
     *
     * @since 1.0.0
     */
    public static <T> T getVolatile(final FieldAddress<T> address) {
        address.validate(); // Implicit null-check

        final Object o = address.owner();
        final long offset = address.offset();
        final Class<T> type = address.type();

        if (type == boolean.class) {
            return address.cast(Unsafe.getBooleanVolatile(o, offset));
        }
        if (type == byte.class) {
            return address.cast(Unsafe.getByteVolatile(o, offset));
        }
        if (type == char.class) {
            return address.cast(Unsafe.getCharVolatile(o, offset));
        }
        if (type == double.class) {
            return address.cast(Unsafe.getDoubleVolatile(o, offset));
        }
        if (type == float.class) {
            return address.cast(Unsafe.getFloatVolatile(o, offset));
        }
        if (type == int.class) {
            return address.cast(Unsafe.getIntVolatile(o, offset));
        }
        if (type == long.class) {
            return address.cast(Unsafe.getLongVolatile(o, offset));
        }
        if (type == short.class) {
            return address.cast(Unsafe.getShortVolatile(o, offset));
        }
        if (Object.class.isAssignableFrom(type)) {
            return address.cast(Unsafe.getReferenceVolatile(o, offset));
        }

        throw new IllegalArgumentException("Unknown or unsupported type " + type.getName() + " for field address " + address);
    }

    /**
     * Sets the value of the field or array element pointed to by the given {@code address} with <em>volatile</em>
     * semantics.
     *
     * <p>The given {@link FieldAddress} must be valid, i.e. point to a valid field or array location. Note that the
     * performed checks are only superficial to incur in too big of a performance penalty; the client should thus take
     * care to avoid giving invalid data to this method to prevent crashes in native code.</p>
     *
     * <p>The value will be written according to <em>volatile</em> semantics.</p>
     *
     * @param address The {@link FieldAddress} whose value should be set.
     * @param value The value to set the given field or array element to.
     * @param <T> The type contained in the field or array element.
     * @throws NullPointerException If the {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the {@link FieldAddress} does not point to a valid location.
     * @throws IllegalArgumentException If the type of the field cannot be properly determined or obtained. Note that
     * this exception should never be thrown in normal circumstances.
     *
     * @since 1.0.0
     */
    public static <T> void putVolatile(final FieldAddress<T> address, final T value) {
        address.validate(); // Implicit null-check

        final Object o = address.owner();
        final long offset = address.offset();
        final Class<T> type = address.type();

        if (type == boolean.class) {
            Unsafe.putBooleanVolatile(o, offset, (boolean) value);
        } else if (type == byte.class) {
            Unsafe.putByteVolatile(o, offset, (byte) value);
        } else if (type == char.class) {
            Unsafe.putCharVolatile(o, offset, (char) value);
        } else if (type == double.class) {
            Unsafe.putDoubleVolatile(o, offset, (double) value);
        } else if (type == float.class) {
            Unsafe.putFloatVolatile(o, offset, (float) value);
        } else if (type == int.class) {
            Unsafe.putIntVolatile(o, offset, (int) value);
        } else if (type == long.class) {
            Unsafe.putLongVolatile(o, offset, (long) value);
        } else if (type == short.class) {
            Unsafe.putShortVolatile(o, offset, (short) value);
        } else if (Object.class.isAssignableFrom(type)) {
            Unsafe.putReferenceVolatile(o, offset, value);
        } else {
            throw new IllegalArgumentException("Unknown or unsupported type " + type.getName() + " for field address " + address);
        }
    }



    /**
     * Sets the value of the field or array element pointed to by the given {@code address} with
     * <em>release/acquire</em> semantics.
     *
     * <p>The given {@link FieldAddress} must be valid, i.e. point to a valid field or array location. Moreover, only
     * a few types can be set with <em>release/acquire</em> semantics: {@code int}s, {@code long}s, and references. Any
     * other type will be refused. Note that the performed checks are only superficial to incur in too big of a
     * performance penalty; the client should thus take care to avoid giving invalid data to this method to prevent
     * crashes in native code.</p>
     *
     * <p>The value will be written according to <em>release/acquire</em> semantics.</p>
     *
     * @param address The {@link FieldAddress} whose value should be set.
     * @param value The value to set the given field or array element to.
     * @param <T> The type contained in the field or array element.
     * @throws NullPointerException If the {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the {@link FieldAddress} does not point to a valid location.
     * @throws IllegalArgumentException If the type of the field cannot be set according to <em>release/acquire</em>
     * semantics.
     *
     * @since 1.0.0
     */
    public static <T> void putRelease(final FieldAddress<T> address, final T value) {
        address.validate(); // Implicit null-check

        final Object o = address.owner();
        final long offset = address.offset();
        final Class<T> type = address.type();

        if (type == int.class) {
            Unsafe.putIntRelease(o, offset, (int) value);
        } else if (type == long.class) {
            Unsafe.putLongRelease(o, offset, (long) value);
        } else if (Object.class.isAssignableFrom(type)) {
            Unsafe.putReferenceRelease(o, offset, value);
        } else {
            throw new IllegalArgumentException("Unknown or unsupported type " + type.getName() + " for field address " + address);
        }
    }



    /**
     * Unparks the given {@link Thread}, if {@linkplain #park(boolean, long) parked}.
     *
     * <p>If the {@code Thread} was not parked previously, then the subsequent call to {@link #park(boolean, long)} will
     * be ignored. Essentially, the invocation of this method will be "remembered" until the next time the thread has
     * been parked.</p>
     *
     * @param thread The {@link Thread} that should not unparked.
     *
     * @since 1.0.0
     */
    @SuppressWarnings("SpellCheckingInspection")
    public static void unpark(final Thread thread) {
        Unsafe.unpark(thread);
    }

    /**
     * Parks the current {@link Thread}, blocking its execution until this method returns.
     *
     * <p>In other words, a {@code Thread} should be considered parked only if it currently "busy" executing this
     * method. A thread will thus exit the parked state when this method returns. In turn, this means that the thread
     * will not be blocked anymore and will be free to continue execution of its code.</p>
     *
     * <p>If the given {@code Thread} had an {@linkplain #unpark(Thread) un-park request} "pending", then this method
     * will return immediately. Otherwise, the thread will park until either a corresponding un-park request occurs or,
     * if {@code time} has a non-zero value, either the specified amount of {@code time} passes (if {@code absolute} is
     * {@code false}) or the given Epoch time is hit (if {@code absolute} is {@code true}).</p>
     *
     * <p>Take note that a thread might also exit its parked state if it gets
     * {@linkplain Thread#interrupt() interrupted} or for any other spurious reasons as determined by the scheduler.</p>
     *
     * @param isAbsolute Indicates whether {@code time} should be treated as an Epoch instant ({@code true}) or as an
     *                   amount of milliseconds ({@code false}). This value is ignored if {@code time} is {@code 0}.
     * @param time The maximum amount of time that the {@link Thread} should remain in its parked state, or the Epoch
     *             at which the {@code Thread} should resume execution, or {@code 0}. The last value indicates that no
     *             limit on the amount of time has been placed.
     *
     * @since 1.0.0
     */
    public static void park(final boolean isAbsolute, final long time) {
        Unsafe.park(isAbsolute, time);
    }



    /**
     * Obtains the load average in the system run queue assigned to the available processors averaged over various
     * periods of time.
     *
     * <p>The amount of samples retrieved is given by {@code nSamples} and the values are placed in the
     * {@code loadAverage} array from index {@code 0} to {@code nSamples - 1}.</p>
     *
     * <p>It is not allowed to request more than {@code 3} samples, corresponding to averages over the last 1, 5, and 15
     * minutes respectively. Note that the system might always return less samples than the requested ones: callers of
     * this method should always verify the return value.</p>
     *
     * @param loadAverage The array in which the load average samples should be stored in.
     * @param nSamples The amount of samples to retrieve: it must be between {@code 1} and {@code 3} inclusive.
     * @return The amount of samples actually retrieved, or {@code -1} if the load average cannot be obtained.
     * @throws NullPointerException If {@code loadAverage} is {@code null}.
     * @throws IllegalArgumentException If {@code nSamples} is outside the range of validity.
     * @throws ArrayIndexOutOfBoundsException If {@code nSamples} is bigger than the length of {@code loadAverage}.
     * @throws UnsupportedOperationException If the current system prevents computation of load averages.
     *
     * @since 1.0.0
     */
    public static int getLoadAverage(final double[] loadAverage, final int nSamples) {
        if (nSamples < 1 || nSamples > 3) {
            throw new IllegalArgumentException("Load average value must be between 1 and 3 inclusive");
        }

        final int returnedSamples = Unsafe.getLoadAverage(loadAverage, nSamples);

        if (returnedSamples == -1) {
            throw new UnsupportedOperationException("Load average computation failed");
        }

        return returnedSamples;
    }



    /**
     * Atomically obtains the value of the field or array element pointed to by the given {@code address} and adds the
     * given {@code delta} to it.
     *
     * <p>The given {@link FieldAddress} must be valid, i.e. pointing to a valid field or array location. Moreover, only
     * a certain set of types can be operated upon in this way: {@code int}s and {@code long}s. Any other types will
     * cause an error to be raised. Note that the checks performed are only superficial to avoid incurring in too big of
     * a performance penalty: the client should thus take care to avoid giving invalid data to this method to prevent
     * crashes in native code.</p>
     *
     * <p>Both the read operation and the addition will be performed in a single atomic operation.</p>
     *
     * @param address The {@link FieldAddress} of the field whose value should be obtained and modified.
     * @param delta The amount that should be added to the value.
     * @return The value of the pointed field or array element before the update operation occurred.
     * @param <T> The type of the value contained in the pointed field or array element.
     * @throws NullPointerException If the {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the {@link FieldAddress} does not point to a valid location.
     * @throws IllegalArgumentException If the type of the field cannot be operated upon with this method.
     *
     * @since 1.0.0
     */
    public static <T> T getAndAdd(final FieldAddress<T> address, final T delta) {
        address.validate(); // Implicit null-check

        final Object o = address.owner();
        final long offset = address.offset();
        final Class<T> type = address.type();

        if (type == int.class) {
            return address.cast(Unsafe.getAndAddInt(o, offset, (int) delta));
        } else if (type == long.class) {
            return address.cast(Unsafe.getAndAddLong(o, offset, (long) delta));
        } else {
            throw new IllegalArgumentException("Unknown or unsupported type " + type.getName() + " for field address " + address);
        }
    }

    /**
     * Atomically exchanges the value of the field or array element pointed to by the given {@code address} with a new
     * value.
     *
     * <p>The given {@link FieldAddress} must be valid, i.e. pointing to a valid field or array location. Moreover, only
     * a certain set of types can be operated upon in this way: {@code int}s and {@code long}s. Any other types will
     * cause an error to be raised. Note that the checks performed are only superficial to avoid incurring in too big of
     * a performance penalty: the client should thus take care to avoid giving invalid data to this method to prevent
     * crashes in native code.</p>
     *
     * <p>The exchange operation is performed atomically, with the old value being returned by this method.</p>
     *
     * @param address The {@link FieldAddress} of the field whose value should be obtained and modified.
     * @param newValue The new value that should be set.
     * @return The value of the pointed field or array element before the exchange operation occurred.
     * @param <T> The type of the value contained in the pointed field or array element.
     * @throws NullPointerException If the {@link FieldAddress} is {@code null}.
     * @throws IllegalStateException If the {@link FieldAddress} does not point to a valid location.
     * @throws IllegalArgumentException If the type of the field cannot be operated upon with this method.
     *
     * @since 1.0.0
     */
    public static <T> T getAndSet(final FieldAddress<T> address, final T newValue) {
        address.validate(); // Implicit null-check

        final Object o = address.owner();
        final long offset = address.offset();
        final Class<T> type = address.type();

        if (type == int.class) {
            return address.cast(Unsafe.getAndSetInt(o, offset, (int) newValue));
        } else if (type == long.class) {
            return address.cast(Unsafe.getAndSetLong(o, offset, (long) newValue));
        } else if (Object.class.isAssignableFrom(type)) {
            return address.cast(Unsafe.getAndSetReference(o, offset, newValue));
        } else {
            throw new IllegalArgumentException("Unknown or unsupported type " + type.getName() + " for field address " + address);
        }
    }



    /**
     * Marks a loading fence according to the <em>release/acquire</em> semantics.
     *
     * <p>In particular, no load operations that occur before the fence will be reordered with loads and stores that
     * occur after the fence, applying what is also known as an <em>acquire</em> fence.</p>
     *
     * <p>This method corresponds to the C11 {@code atomic_thread_fence(memory_order_acquire)} operation.</p>
     *
     * @since 1.0.0
     */
    public static void loadFence() {
        Unsafe.loadFence();
    }

    /**
     * Marks a storing fence according to the <em>release/acquire</em> semantics.
     *
     * <p>In particular, no load or store operations that occur before the fence will be reordered with stores that
     * occur after the fence, applying what is also known as a <em>release</em> fence.</p>
     *
     * <p>This method corresponds to the C11 {@code atomic_thread_fence(memory_order_release)} operation.</p>
     *
     * @since 1.0.0
     */
    public static void storeFence() {
        Unsafe.storeFence();
    }

    /**
     * Marks a loading and storing fence (also known as full fence) according to the <em>release/acquire</em> semantics.
     *
     * <p>This method acts as both {@link #loadFence()} and {@link #storeFence()} together, essentially ensuring that
     * no load or store operations that occur before the fence will be reordered with loads and stores that occur after
     * the fence.</p>
     *
     * <p>This method corresponds to the C11 {@code atomic_thread_fence(memory_order_seq_cst)} operation.</p>
     *
     * @since 1.0.0
     */
    public static void fullFence() {
        Unsafe.fullFence();
    }



    /**
     * Invokes the given {@link ByteBuffer}'s {@link java.lang.ref.Cleaner Cleaner} instance, if it exists.
     *
     * <p>The given buffer must be a {@linkplain ByteBuffer#isDirect() direct} {@code ByteBuffer}, and cannot be neither
     * a {@linkplain ByteBuffer#slice() slice} nor a {@linkplain ByteBuffer#duplicate() duplicate}.</p>
     *
     * @param directBuffer The {@link ByteBuffer} whose {@link java.lang.ref.Cleaner Cleaner} should be invoked.
     * @throws NullPointerException If {@code directBuffer} is {@code null}.
     * @throws IllegalArgumentException If {@code directBuffer} is not a {@linkplain ByteBuffer#isDirect() direct}
     * buffer, or if it is either a {@linkplain ByteBuffer#slice() slice} or a
     * {@linkplain ByteBuffer#duplicate() duplicate}.
     *
     * @since 1.0.0
     */
    public void invokeCleaner(final ByteBuffer directBuffer) {
        Unsafe.invokeCleaner(directBuffer);
    }
}
